#!/bin/bash
#
# Install required packages
brew update &&
brew install ripgrep &&

# Install neovim
cd ~/.zsh &&
wget https://github.com/neovim/neovim/releases/download/stable/nvim-macos.tar.gz &&
xattr -c ./nvim-macos.tar.gz &&
tar xzvf nvim-macos.tar.gz &&
echo 'alias nvim="~/.zsh/nvim-macos/bin/nvim"' >> ~/.profile &&

# Install packer.nvim
git clone --depth 1 https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim

# Execute inside neovim
# 1. :PackerInstall
# 2. :LspInfoInstall (jedi)
# 3. :TSInstall
