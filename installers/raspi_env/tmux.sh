#!/bin/bash

# Install dependencies
sudo -E apt install -y automake bison build-essential pkg-config libevent-dev libncurses5-dev &&

# Clone and Install tmux
sudo -E git clone https://github.com/tmux/tmux /usr/local/src/tmux &&
cd /usr/local/src/tmux &&
sudo -E ./autogen.sh &&
sudo -E ./configure --prefix=/usr/local &&
sudo -E make &&
sudo -E make install &&

# Clone tpm
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
