# Noevim packages

* General

| Name | Description |
| --- | --- |
| [wbthomason/packer.nvim](https://github.com/wbthomason/packer.nvim) | Pakcage installer for neovim |


* Movement related

| Name | Description |
| --- | --- |
| [petertriho/nvim-scrollbar](https://github.com/petertriho/nvim-scrollbar) | Extensible Neovim Scrollbar |
| [kevinhwang91/nvim-hlslens](https://github.com/kevinhwang91/nvim-hlslens) | nvim-hlslens helps you better glance at matched information, seamlessly jump between matched instances |
| [unblevable/quick-scope](https://github.com/unblevable/quick-scope) | An always-on highlight for a unique character in every word on a line to help you use f, F and family (disabled) |


* Edit related

| Name | Description |
| --- | --- |
| [ntpeters/vim-better-whitespace](https://github.com/ntpeters/vim-better-whitespace) | Highlight all trailing whitespaces as red |
| [vim-tmux-navigator](https://github.com/christoomey/vim-tmux-navigator) | Navigate seamlessly between vim and tmux |
| [ervandew/supertab](https://github.com/ervandew/supertab) | Use tab for completion (disabled) |
| [terryma/vim-expand-region](https://github.com/terryma/vim-expand-region) | Expand selected region by pressing `v` multiple times|
| [mg979/vim-visual-multi](https://github.com/mg979/vim-visual-multi) | Edit with multiple cursors |
| [yamatsum/nvim-cursorline](https://github.com/yamatsum/nvim-cursorline) | Highlight words and lines on the cursor |
| [folke/todo-comments.nvim](https://github.com/folke/todo-comments.nvim) | highlight and search for todo comments like TODO, HACK, BUG |
| [mvllow/modes.nvim](https://github.com/mvllow/modes.nvim) | Prismatic line decorations for the adventurous vim user |
| [kana/vim-niceblock](https://github.com/kana/vim-niceblock) | Improves performance of visual block |
| [numToStr/Comment.nvim](https://github.com/numToStr/Comment.nvim) |  Smart and Powerful commenting plugin for neovim
| [rmagatti/goto-preview](https://github.com/rmagatti/goto-preview) | A small Neovim plugin for previewing native LSP's goto definition, type definition, implementation, and references calls in floating windows.


* Visual related

| Name | Description |
| --- | --- |
| [EdenEast/nightfox.nvim](https://github.com/EdenEast/nightfox.nvim) | Nightfox themes |
| [kyazdani42/nvim-web-devicons](https://github.com/nvim-tree/nvim-web-devicons) | Enable use of icons in neovim |
| [ryanoasis/vim-devicons](https://github.com/ryanoasis/vim-devicons) |  Enable use of icons in neovim |
| [vim-airline/vim-airline-themes](https://github.com/vim-airline/vim-airline-themes) | Show airline at the bottom of the editor (disabled) |
| [nvim-lualine/lualine.nvim](https://github.com/nvim-lualine/lualine.nvim) | A blazing fast and easy to configure Neovim statusline written in Lua. |
| [folke/tokyonight.nvim](https://github.com/folke/tokyonight.nvim) | TokyoNignt theme |
| [akinsho/bufferline.nvim](https://github.com/akinsho/bufferline.nvim) |
| [rcarriga/nvim-notify](https://github.com/rcarriga/nvim-notify) | Decorated notifier in neovim | 
| [lukas-reineke/indent-blankline.nvim](https://github.com/lukas-reineke/indent-blankline.nvim) | adds indentation guides to all lines (including empty lines)
| [VonHeikemen/searchbox.nvim](https://github.com/VonHeikemen/searchbox.nvim) | Start your search from a more comfortable place


* Git related

| Name | Description |
| --- | --- |
| [tpope/vim-fugitive](https://github.com/tpope/vim-fugitive) | :Git  commands |
| [TimUntersberger/neogit](https://github.com/TimUntersberger/neogit) | Git controller window |
| [sindrets/diffview.nvim](https://github.com/sindrets/diffview.nvim) | Single tabpage interface for easily cycling through diffs for all modified files for any git rev
| [akinsho/git-conflict.nvim](https://github.com/akinsho/git-conflict.nvim) | A plugin to visualise and resolve conflicts in neovim
| [rhysd/committia.vim](https://github.com/rhysd/committia.vim) | More Pleasant Editing on Commit Message (disabled)
| [lewis6991/gitsigns.nvim](https://github.com/lewis6991/gitsigns.nvim) | Super fast git decorations implemented purely in lua/teal.

* Filer

| Name | Description |
| --- | --- |
| [nvim-neo-tree/neo-tree.nvim](https://github.com/nvim-neo-tree/neo-tree.nvim) | plugin to browse the file system and other tree like structures |

* Fuzzy finders

| Name | Description |
| --- | --- |
| [nvim-telescope/telescope.nvim](https://github.com/nvim-telescope/telescope.nvim) | Highly extendable fuzzy finder over lists


* Treesitter

| Name | Description |
| --- | --- |
| [nvim-treesitter/nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter) | Tree-sitter is a parser generator tool and an incremental parsing library |


* Sidebar

| Name | Description |
| --- | --- |
| [sidebar-nvim/sidebar.nvim](https://github.com/sidebar-nvim/sidebar.nvim) | A generic and modular lua sidebar inspired by lualine |
| [sidebar-nvim/sections-dap](https://github.com/sidebar-nvim/sections-dap) | Dap Sections for sidebar.nvim |


* Outline

| Name | Description |
| --- | --- |
| [stevearc/aerial.nvim](https://github.com/stevearc/aerial.nvim) | A code outline window for skimming and quick navigation |


* LSP and autofill related

| Name | Description |
| --- | --- |
| [hrsh7th/nvim-cmp](https://github.com/hrsh7th/nvim-cmp) | 
A completion engine plugin for neovim written in Lua. Completion sources are installed from external repositories and "sourced" |
| [neovim/nvim-lspconfig](https://github.com/neovim/nvim-lspconfig) | Configs for the Nvim LSP client | 
| [hrsh7th/cmp-buffer](https://github.com/hrsh7th/cmp-buffer) | nvim-cmp source for buffer words |
| [hrsh7th/cmp-calc](https://github.com/hrsh7th/cmp-calc) | nvim-cmp source for math calculation |
| [hrsh7th/cmp-cmdline](https://github.com/hrsh7th/cmp-cmdline) | nvim-cmp source for vim's cmdline |
| [hrsh7th/cmp-emoji](https://github.com/hrsh7th/cmp-emoji) | nvim-cmp source for emojis (disabled) |
| [hrsh7th/cmp-nvim-lsp](https://github.com/hrsh7th/cmp-nvim-lsp) | nvim-cmp source for neovim's built-in language server client |
| [hrsh7th/cmp-nvim-lsp-signature-help](https://github.com/hrsh7th/cmp-nvim-lsp-signature-help) | nvim-cmp source for displaying function signatures with the current parameter emphasized |
| [hrsh7th/cmp-path](https://github.com/hrsh7th/cmp-path) | nvim-cmp source for filesystem paths |
| [hrsh7th/cmp-nvim-lua](https://github.com/hrsh7th/cmp-nvim-lua) | nvim-cmp source for neovim Lua API |
| [onsails/lspkind.nvim](https://github.com/onsails/lspkind.nvim) | This tiny plugin adds vscode-like pictograms to neovim built-in lsp |
| [ray-x/cmp-treesitter](https://github.com/ray-x/cmp-treesitter) |nvim-cmp source for treesitter nodes |
| [rcarriga/cmp-dap](https://github.com/rcarriga/cmp-dap) | nvim-cmp source for nvim-dap REPL and nvim-dap-ui buffers |
| [williamboman/mason.nvim](https://github.com/williamboman/mason.nvim) | Portable package manager for Neovim that runs everywhere Neovim runs.
Easily install and manage LSP servers, DAP servers, linters, and formatters |
| [williamboman/mason-lspconfig.nvim](https://github.com/williamboman/mason-lspconfig.nvim) | mason-lspconfig.nvim closes some gaps that exist between mason.nvim and lspconfig |
| [jay-babu/mason-nvim-dap.nvim](https://github.com/jay-babu/mason-nvim-dap.nvim) | mason-nvim-dap bridges mason.nvim with the nvim-dap plugin |
| [tzachar/cmp-tabnine](https://github.com/tzachar/cmp-tabnine) | Tabnine source for hrsh7th/nvim-cmp |


* LSP related UI

| Name | Description |
| --- | --- |
| [tami5/lspsaga.nvim](https://github.com/nvimdev/lspsaga.nvim) | Imporved LSP UI | 
| [folke/lsp-colors.nvim](https://github.com/folke/lsp-colors.nvim) | Automatically creates missing LSP diagnostics highlight groups for color schemes |
| [j-hui/fidget.nvim](https://github.com/j-hui/fidget.nvim) | Standalone UI for nvim-lsp progress. Eye candy for the impatient. |
| [folke/trouble.nvim](https://github.com/folke/trouble.nvim) | A pretty list for showing diagnostics, references, telescope results, quickfix and location lists


* Debug

| Name | Description |
| --- | --- |
| [mfussenegger/nvim-dap](https://github.com/mfussenegger/nvim-dap) | Debug Adapter Protocol client implementation for Neovim
| [rcarriga/nvim-dap-ui](https://github.com/rcarriga/nvim-dap-ui) | A UI for nvim-dap which provides a good out of the box configuration.
| [theHamsta/nvim-dap-virtual-text](https://github.com/theHamsta/nvim-dap-virtual-text) | This plugin adds virtual text support to nvim-dap.


* Terminal

| Name | Description |
| --- | --- |
| [akinsho/toggleterm.nvim](https://github.com/akinsho/toggleterm.nvim) | A neovim plugin to persist and toggle multiple terminals during an editing session|


* Runner

| Name | Description |
| --- | --- |
| [michaelb/sniprun](https://github.com/michaelb/sniprun) | Code runner plugin for neovim written in Lua and Rust.


* Other

| Name | Description |
| --- | --- |
| [tyru/open-browser.vim](https://github.com/tyru/open-browser.vim) | Open URI with your favorite browser from your most favorite editor.
| [tyru/open-browser-github.vim](https://github.com/tyru/open-browser-github.vim) | Opens GitHub URL of current file, etc. from Vim
