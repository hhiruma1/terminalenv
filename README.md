# My terminal environemnt
Zsh + Tmux + Neovim + Packer.nvim

## Quick start
1. sudo apt update
2. sudo apt install -y git make
3. git clone http://gitlab.com/hhiruma1/terminalenv.git ~/terminalenv

## Build your environment with `make` command
* Make commands defined at: [./Makefile](./Makefile)
* Usage
    * Specify environment type and the feature you want to install
        * e.g. `make apt_env base neovim docker`
        * e.g. `make docker_env base tmux neovim`
    * Environment types (at least one must be specified):
        | Environment | Description |
        | --- | --- |
        | apt_env | Host ubuntu environment (which requres sudo for apt install) | 
        | docker_env | Docker ubuntu environment (which DOES NOT requre sudo for apt install) |
        | homebrew_env | MacOS with homebrew environment | 
    * Features:
        | Feature | Installing packages |
        | --- | --- |
        | base | Zsh, nerdfont, exa, vivid, powerlevel10k, fzf | 
        | neovim | Neovim, Packer.nvim, ripgrep |
        | tmux | tmux, tpm | 
        | docker | docker, docker-ce, docker compose | 
        | pyenv | pyenv, pyenv-virtualenv | 
        | node | nvm, node.js | 

