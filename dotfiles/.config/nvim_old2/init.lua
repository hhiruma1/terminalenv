require 'plugins'
require 'plugin_settings/notify'

--#####################
-- Packer install settings
--#####################
local packer_install_path = '~/.local/share/nvim/site/pack/packer/start/packer.nvim'

if vim.fn.empty(vim.fn.glob(packer_install_path)) > 0 then
  vim.fn.execute(
    '!git clone https://github.com/wbthomason/packer.nvim ' .. packer_install_path
  )
end

--#####################
-- Basic Settings
--#####################
-----------------------
-- Key Mapping related
-----------------------
vim.g.mapleader = ' '

--Split view
vim.api.nvim_set_keymap('n', '<Leader>s', ':split<CR>', {noremap=true})
vim.api.nvim_set_keymap('n', '<Leader>v', ':vsplit<CR>', {noremap=true})
--Toggle NERDTree
vim.api.nvim_set_keymap('n', '<Leader>n', ':Neotree toggle<CR>', {noremap=true, silent=true})
--Move between buffers
vim.api.nvim_set_keymap('n', '<Leader>[', ':bprev<CR>', {noremap=true, silent=true})
vim.api.nvim_set_keymap('n', '<Leader>]', ':bnext<CR>', {noremap=true, silent=true})
--Delete buffer
vim.api.nvim_set_keymap('n', '<Leader>dd', ':bd<CR>', {noremap=true, silent=true})
--Move visual lines
vim.api.nvim_set_keymap('n', 'j', 'gj', {noremap=true})
vim.api.nvim_set_keymap('n', 'k', 'gk', {noremap=true})
--Expand visual region
vim.api.nvim_set_keymap('v', 'v', '<Plug>(expand_region_expand)', {noremap=true})
vim.api.nvim_set_keymap('v', 'V', '<Plug>(expand_region_shrink)', {noremap=true})
--Resize windows
vim.api.nvim_set_keymap('n', '<Leader>w', '<C-w>=', {noremap=true})


-----------------------
-- Visual configurations
-----------------------
--Line numbers
vim.opt.number = true
vim.wo.signcolumn = "yes"
--Enable cursor to move one char ahead
vim.opt.virtualedit = 'onemore'
--Smartindent
vim.opt.smartindent = true
--Matching brackets
vim.opt.showmatch = true
--show folder icons
vim.g.WebDevIconsUnicodeDecorateFolderNodes = 1
--Use opened buffer instead of recreating
vim.opt.switchbuf = 'useopen'
--Show currently inputting commands
vim.opt.showcmd = true
--Show current input mode
vim.opt.showmode = true
--Colorscheme
vim.api.nvim_command [[colorscheme nightfox]]
--Darkmode
vim.opt.background = 'dark'
--Enable syntax highlight
vim.api.nvim_command [[syntax on]]
--Disable wrapping
vim.opt.wrap = false
--Termguicolors
vim.opt.termguicolors = true


-----------------------
-- Editing related
-----------------------
--Visualize unseen characters
vim.opt.listchars = {tab='▸-'}
--Convert tab as multiple spaces
vim.opt.expandtab = true
--Other configs
vim.opt.tabstop = 2
vim.opt.shiftwidth = 4
vim.opt.autoindent = true
vim.opt.smartindent = true
--Enable backspace
vim.opt.backspace = {'indent', 'eol', 'start'}
--Set clipboard
vim.opt.clipboard:append{'unnamedplus'}


-----------------------
-- Search related
-----------------------
--Ignore letter cases when searched with all lower cases
vim.opt.ignorecase = true
--Differentiate case when searched including upper cases
vim.opt.smartcase = true
--Highlight during input
vim.opt.incsearch = true
--Return when reached the EOF
vim.opt.wrapscan = true
--Highlight targets
vim.opt.hlsearch = true
--Escape to end highlight
vim.api.nvim_set_keymap('n', '<Esc><Esc>', ':nohlsearch<CR>', {})


-----------------------
-- Other settings
-----------------------
--mouse controll
vim.opt.mouse = 'a'
--Round shifting using < and > to shiftwidth
vim.opt.shiftround = true
--Ignore case on completion
vim.opt.infercase = true
--set file encodings
vim.opt.fileencodings = 'utf-8', 'cp932', 'euc-jp', 'sjis'
--Do not create backups or swapfiles
vim.opt.writebackup = false
vim.opt.backup = false
vim.opt.swapfile = false
--Auto reload file if edited
vim.opt.autoread = true
--Disable beep sound
vim.opt.belloff = 'all'


-----------------------
-- Plugin settings
-----------------------
--LSP
local cmp = require('cmp')
local lspkind = require('lspkind')
local mason = require('mason')
local lspconfig = require('lspconfig')
local tabnine = require('cmp_tabnine.config')
local compare = require('cmp.config.compare')
local source_mapping = {
    buffer = "[Buffer]",
    nvim_lsp = "[LSP]",
    nvim_lua = "[Lua]",
    cmp_tabnine = "[TN]",
    path = "[Path]",
    cmdline = '[CMD]',
    dap = '[DAP]',
    treesitter = '[TS]',
}

tabnine:setup({
    max_lines = 1000;
    max_num_results = 20;
    sort = true;
    run_on_every_keystroke = true;
    snippet_placeholder = '..';
    --ignored_file_types = {};
    show_prediction_strength = true;
})

local mason = mason.setup({
   ui = {
     icons = {
       package_installed = "✓",
       package_pending = "➜",
       package_uninstalled = "✗"
     }
   }
 })

require('mason-nvim-dap').setup({
    ensure_installed = {'python'}
})

vim.opt.completeopt = {'menu', 'menuone', 'noselect'}
cmp.setup {
    snippet = {},
    window = {
        completion = cmp.config.window.bordered(),
    },
    mapping = {
        ['<C-p>'] = cmp.mapping.select_prev_item(),
        ['<C-n>'] = cmp.mapping.select_next_item(),
        ['<CR>'] = cmp.mapping.confirm {
            select = true,
        },
    },
    sources = {
        {name = 'cmp_tabnine'},
        {name = 'nvim_lsp'},
        {name = 'nvim_lsp_signature_help'},
        {name = 'treesitter'},
        {name = 'path'},
        {name = 'dap'},
        {name = 'buffer'},
        {name = 'cmdline'},
        {name = 'nvim_lua'},
        {name = 'emoji'},
        {name = 'calc'},
    },
    formatting = {
        format = lspkind.cmp_format({
        mode = 'symbol_text',
        maxwidth = 100,
        before = function (entry, vim_item)
            vim_item.kind = lspkind.presets.default[vim_item.kind]

            local menu = source_mapping[entry.source.name]
            if entry.source.name == "cmp_tabnine" then
            if entry.completion_item.data ~= nil and entry.completion_item.data.detail ~= nil then
                menu = entry.completion_item.data.detail .. " " .. menu
            end
                vim_item.kind = ""
            end

            vim_item.menu = menu
            return vim_item
        end
        })
    },
    enabled = function ()
        return vim.api.nvim_buf_get_option(0, 'buftype') ~= 'prompt'
        or require('cmp_dap').is_dap_buffer()
    end,
    sorting = {
        priority_weight = 2,
        comparators = {
            require('cmp_tabnine.compare'),
            compare.offset,
            compare.exact,
            compare.score,
            compare.recently_used,
            compare.kind,
            compare.sort_text,
            compare.length,
            compare.order,
        },
    },
}

cmp.setup.cmdline('/', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = {
        { name = 'buffer' }
    }
})
cmp.setup.cmdline(':', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({
        { name = 'path' }
    }, {
        { name = 'cmdline' }
    })
})

local opts = { noremap = true, silent = true }
vim.api.nvim_set_keymap("n", "K", "<cmd>lua vim.lsp.buf.hover()<CR>", opts)
vim.api.nvim_set_keymap("n", "<LEADER>jd", "<cmd>lua require'telescope.builtin'.lsp_definitions()<CR>", opts)
vim.api.nvim_set_keymap("n", "<LEADER>jv", "<cmd>lua require'telescope.builtin'.lsp_definitions({jump_type='vsplit'})<CR>", opts)
vim.api.nvim_set_keymap("n", "<LEADER>jr", "<cmd>lua require'telescope.builtin'.lsp_references()<CR>", opts)
vim.api.nvim_set_keymap("n", "<LEADER>rn", "<cmd>lua vim.lsp.buf.rename()<CR>", opts)
vim.api.nvim_set_keymap("n", "<LEADER>aa", "<cmd>lua vim.lsp.buf.code_action()<CR>", opts)
local capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())

local mason_lspconfig = require('mason-lspconfig')
mason_lspconfig.setup {
    -- ensure_installed = {'jedi_language_server', 'zk'},
    ensure_installed = {'zk'},
    automatic_installation = true,
}
mason_lspconfig.setup_handlers({
    function(server_name)
        local opts = {}
        opts.settings = {
            on_attach = on_attach,
            capabilities = capabilities,
        }
        lspconfig[server_name].setup(opts)
    end
})

--LSP UI
require("trouble").setup{}
vim.api.nvim_set_keymap('n', '<Leader>xx', '<cmd>TroubleToggle<cr>', {noremap=true})
vim.api.nvim_buf_set_keymap(0, "n", "gr", "<cmd>Lspsaga rename<cr>", {silent = true, noremap = true})
local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
for type, icon in pairs(signs) do
  local hl = "DiagnosticSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end

--Spell
vim.opt.spell = true
vim.opt.spelllang = {'en_us'}

--Fuzzy finders
vim.api.nvim_set_keymap('n', '<Leader>ff', '<cmd>Telescope find_files hidden=true prompt_prefix=🔍 <cr>', {noremap=true})
vim.api.nvim_set_keymap('n', '<Leader>fg', '<cmd>Telescope live_grep prompt_prefix=🔍 <cr>', {noremap=true})
vim.api.nvim_set_keymap('n', '<Leader>fb', '<cmd>Telescope buffers prompt_prefix=🔍 <cr>', {noremap=true})
vim.api.nvim_set_keymap('n', '<Leader>fh', '<cmd>Telescope help_tags prompt_prefix=🔍 <cr>', {noremap=true})
vim.api.nvim_set_keymap('n', '<Leader>fr', '<cmd>Telescope lsp_references prompt_prefix=🔍 <cr>', {noremap=true})
vim.api.nvim_set_keymap('n', '<Leader>fd', '<cmd>Telescope find_files cwd=~/terminalenv/dotfiles hidden=true <cr>', {noremap=true})
vim.api.nvim_set_keymap('n', '<Leader>ft', '<cmd>Telescope find_files cwd=~/envtips <cr>', {noremap=true})

--Treesitter
require('nvim-treesitter.configs').setup {
    ensure_installed = {'python', 'lua'},
    sync_install = false,
    highlight = {enable = true,},
}

--Visuals
require('lualine').setup{
    options={
        component_separators={ left = '', right = '' },
        section_separators={ left = '', right = '' },
    },
}

--Bufferline
require("bufferline").setup{
    options={
        diagnostics='nvim_lsp',
        diagnostics_update_in_insert=true,
        diagnostics_indicator = function(count, level, diagnostics_dict, context)
            local icon = level:match("error") and " " or " "
            return " " .. icon .. count
        end,
        offsets = {{filetype="neo-tree", text="File Explorer", text_align="left"}},
    }
}

--Highlight
require("todo-comments").setup{
  keywords = {
    FIX = { icon = " ", color = "error", alt = { "FIXME", "BUG", "FIXIT", "ISSUE" }, },
    TODO = { icon = " ", color = "info", alt = { "ADD" } },
    HACK = { icon = " ", color = "warning", alt = { "DEBUG", "TMP" } },
    WARN = { icon = " ", color = "warning", alt = { "WARNING", "XXX" } },
    PERF = { icon = " ", alt = { "OPTIM", "PERFORMANCE", "OPTIMIZE" } },
    NOTE = { icon = " ", color = "hint", alt = { "INFO", "CHECK" } },
  },
}
require('modes').setup{}

--Sidebar
require('sidebar-nvim').setup({
    open=false,
    side='right',
    hide_statusline=true,
    sections = { 'datetime', 'git', 'diagnostics', 'buffers', require("dap-sidebar-nvim.breakpoints")},
    buffers = {
        icon = '',
        ignored_buffers = {}, -- ignore buffers by regex
        sorting = 'id', -- alternatively set it to "name" to sort by buffer name instead of buf id
        show_numbers = true, -- whether to also show the buffer numbers
    },
    dap = {
        breakpoints = {
            icon = "🔍"
        }
    },
})
vim.api.nvim_set_keymap('n', '<Leader>b', '<cmd>SidebarNvimToggle<cr>', {noremap=true})

--Scroll
local colors = require("tokyonight.colors").setup()
require("scrollbar").setup{
    handle = {
        color = colors.bg_highlight,
    },
    marks = {
        Search = { color = colors.orange },
        Error = { color = colors.error },
        Warn = { color = colors.warning },
        Info = { color = colors.info },
        Hint = { color = colors.hint },
        Misc = { color = colors.purple },
    }
}
require("scrollbar.handlers.search").setup()

--Filer
vim.fn.sign_define("DiagnosticSignError", {text = " ", texthl = "DiagnosticSignError"})
vim.fn.sign_define("DiagnosticSignWarn", {text = " ", texthl = "DiagnosticSignWarn"})
vim.fn.sign_define("DiagnosticSignInfo", {text = " ", texthl = "DiagnosticSignInfo"})
vim.fn.sign_define("DiagnosticSignHint", {text = "", texthl = "DiagnosticSignHint"})
require("neo-tree").setup{
    window = {
        width = 30
    },
    filesystem = {
        window = {
            mappings = {
                ['u'] = 'navigate_up'
            }
        },
        filtered_items = {
            visible = true,
            hide_dotfiles = false,
            hide_gitignored = false,
            hide_hidden = false,
        },
    },
    icon = {
        folder_closed = "",
        folder_open = "",
        folder_empty = "ﰊ",
        -- The next two settings are only a fallback, if you use nvim-web-devicons and configure default icons there
        -- then these will never be used.
        default = "*",
        highlight = "NeoTreeFileIcon"
    },
    modified = {
        symbol = "[+]",
        highlight = "NeoTreeModified",
    },
    name = {
        trailing_slash = false,
        use_git_status_colors = true,
        highlight = "NeoTreeFileName",
    },
    git_status = {
        symbols = {
            -- Change type
            added     = "✚", -- or "✚", but this is redundant info if you use git_status_colors on the name
            modified  = "", -- or "", but this is redundant info if you use git_status_colors on the name
            deleted   = "✖",-- this can only be used in the git_status source
            renamed   = "",-- this can only be used in the git_status source
            -- Status type
            untracked = "",
            ignored   = "",
            unstaged  = "",
            staged    = "",
            conflict  = "",
        }
    },
}

--Terminal
require('toggleterm').setup{
    open_mapping=[[<C-\]],
    size = function(term)
        if term.direction == "horizontal" then
            return 15
        elseif term.direction == "vertical" then
            return vim.o.columns * 0.4
        end
    end,
}
vim.api.nvim_buf_set_keymap(0, 't', [[<C-\><C-\>]], [[<C-\><C-n>]], {noremap=true})
vim.api.nvim_buf_set_keymap(0, 't', '<C-H>', [[<C-\><C-n><C-W>h]], {noremap=true})
vim.api.nvim_buf_set_keymap(0, 't', '<C-J>', [[<C-\><C-n><C-W>j]], {noremap=true})
vim.api.nvim_buf_set_keymap(0, 't', '<C-K>', [[<C-\><C-n><C-W>k]], {noremap=true})
vim.api.nvim_buf_set_keymap(0, 't', '<C-L>', [[<C-\><C-n><C-W>l]], {noremap=true})
vim.api.nvim_set_keymap('n', '<Leader>tt', '<cmd>ToggleTerm direction=vertical<CR>', {noremap=true})
vim.api.nvim_set_keymap('n', '<Leader>tv', '<cmd>ToggleTerm direction=vertical<CR>', {noremap=true})
vim.api.nvim_set_keymap('n', '<Leader>th', '<cmd>ToggleTerm direction=horizontal<CR>', {noremap=true})
vim.api.nvim_set_keymap('n', '<Leader>tf', '<cmd>ToggleTerm direction=float<CR>', {noremap=true})

--Reading
require('Comment').setup{
    toggler = { line = ' /', block = ' ?' },
    opleader = { line = ' /', block = ' ?' },
}

--Runner
require('sniprun').setup{
    display = { 'Classic', 'VirtualTextOk', 'NvimNotify', }
}
vim.api.nvim_set_keymap('v', '<Leader>sr', '<Plug>SnipRun', {noremap=true})
vim.api.nvim_set_keymap('n', '<Leader>sc', '<Plug>SnipClose', {noremap=true})

--Outline
require("aerial").setup{
  on_attach = function(bufnr)
    -- Toggle the aerial window with <leader>a
    vim.api.nvim_buf_set_keymap(bufnr, 'n', '<LEADER>o', '<cmd>AerialToggle!<CR>', {})
    -- Jump forwards/backwards with '{' and '}'
    vim.api.nvim_buf_set_keymap(bufnr, 'n', '<LEADER>{', '<cmd>AerialPrev<CR>', {})
    vim.api.nvim_buf_set_keymap(bufnr, 'n', '<LEADER>}', '<cmd>AerialNext<CR>', {})
  end,
  layout = {
    min_width = 20,
  },
}

--Git
require('neogit').setup{
    integrations = {
        diffview = true
    }
}
require('git-conflict').setup()
require('gitsigns').setup{
  signs = {
    add          = {hl = 'GitSignsAdd'   , text = '│', numhl='GitSignsAddNr'   , linehl='GitSignsAddLn'},
    change       = {hl = 'GitSignsChange', text = '│', numhl='GitSignsChangeNr', linehl='GitSignsChangeLn'},
    delete       = {hl = 'GitSignsDelete', text = '_', numhl='GitSignsDeleteNr', linehl='GitSignsDeleteLn'},
    topdelete    = {hl = 'GitSignsDelete', text = '‾', numhl='GitSignsDeleteNr', linehl='GitSignsDeleteLn'},
    changedelete = {hl = 'GitSignsChange', text = '~', numhl='GitSignsChangeNr', linehl='GitSignsChangeLn'},
  },
}
vim.api.nvim_set_keymap('n', '<Leader>g', '<cmd>Neogit<cr>', {noremap=true})
vim.api.nvim_set_keymap('n', '<Leader>gd', '<cmd>DiffviewOpen<cr>', {noremap=true})

--Debug
require('dapui').setup{
    mappings = {
        edit='e', --Edit value of variable
        repl='r', --Send variable to REPL
    },
}
vim.g.dap_virtual_text = true
require('nvim-dap-virtual-text').setup{
    commented = true,
}
require('dap.ext.vscode').load_launchjs('dap_launch.json')
require('dap-python').setup('~/.pyenv/shims/python')
vim.api.nvim_set_keymap('n', '<LEADER>c', '<cmd>lua require"dap".continue()<cr>', {noremap=true})
vim.api.nvim_set_keymap('n', '<LEADER>gl', '<cmd>lua require"dap.ext.vscode".load_launchjs("dap_launch.json")<cr>', {noremap=true})
vim.api.nvim_set_keymap('n', '<LEADER>gb', '<cmd>lua require"dap".toggle_breakpoint()<cr>', {noremap=true})
vim.api.nvim_set_keymap('n', '<LEADER>go', '<cmd>lua require"dap".step_over()<cr>', {noremap=true})
vim.api.nvim_set_keymap('n', '<LEADER>gr', '<cmd>lua require"dap".step_back()<cr>', {noremap=true})
vim.api.nvim_set_keymap('n', '<LEADER>gu', '<cmd>lua require"dap".up()<cr>', {noremap=true})
vim.api.nvim_set_keymap('n', '<LEADER>gd', '<cmd>lua require"dap".down()<cr>', {noremap=true})
vim.api.nvim_set_keymap('n', '<LEADER>gt', '<cmd>lua require"dap".terminate()<cr>', {noremap=true})
vim.api.nvim_set_keymap('n', '<LEADER>gg', '<cmd>lua require"dapui".toggle()<cr>', {noremap=true})
vim.api.nvim_set_keymap('n', '<LEADER>gf', '<cmd>lua require"dapui".float_element("repl")<cr>', {noremap=true})
vim.fn.sign_define('DapBreakpoint', {text='⛔', texthl='', linehl='', numhl=''})
vim.fn.sign_define('DapStopped', {text='👉', texthl='', linehl='', numhl=''})

--Go to preview
require('goto-preview').setup{}
vim.api.nvim_set_keymap('n', '<LEADER>jD', '<cmd>lua require"goto-preview".goto_preview_definition()<CR>', {noremap=true})
vim.api.nvim_set_keymap('n', '<LEADER>jI', '<cmd>lua require"goto-preview".goto_preview_implementation()<CR>', {noremap=true})
vim.api.nvim_set_keymap('n', '<LEADER>jF', '<cmd>lua require"goto-preview".goto_preview_references()<CR>', {noremap=true})
vim.api.nvim_set_keymap('n', '<LEADER>jx', '<cmd>lua require"goto-preview".close_all_win()<CR>', {noremap=true})

--Searchbox
vim.api.nvim_set_keymap('n', '/', ':SearchBoxMatchAll<CR>', {noremap=true})
require('searchbox').setup{
    popup = {
        relative = 'win',
        position = {
            row = '95%',
            col = '95%',
        },
    },
}
