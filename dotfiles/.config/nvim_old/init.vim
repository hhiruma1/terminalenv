"#############################
" Neovim settings
"#############################
let g:python_host_prog='/home/noon/.pyenv/versions/neovim-2/bin/python'
let g:python3_host_prog='/home/noon/.pyenv/versions/neovim-3/bin/python'
let g:node_host_prog=expand('~/.nvm/versions/node/latest/bin/neovim-node-host')


"#############################
" Deinsettings
"#############################
if &compatible
  set nocompatible               " Be iMproved
endif

let s:dein_dir = expand('$HOME/.config/nvim/dein')
let s:dein_repo_dir = s:dein_dir .  '/repos/github.com/Shougo/dein.vim'
let s:toml_dir = expand('$HOME/.config/nvim')

execute 'set runtimepath+=' . s:dein_repo_dir

if dein#load_state(s:dein_dir)
  call dein#begin(s:dein_dir)

  " Load when starting neovim
  call dein#load_toml(s:toml_dir . '/dein.toml', {'lazy': 0})
  " Load when using
  call dein#load_toml(s:toml_dir . '/lazy.toml', {'lazy': 1})

  call dein#end()
  call dein#save_state()
endif

filetype plugin indent on

"if dein#check_install()
  "call dein#install()
"endif


"#############################
" Basic Settings
"#############################
""""""""""""""""""""""""""""""
" Key mapping realted
""""""""""""""""""""""""""""""
let mapleader = "\<Space>"

" Split view
noremap <Leader>s :split<CR>
noremap <Leader>v :vsplit<CR>
" Toggle NERDTree
nnoremap <silent> <Leader>n :NERDTreeToggle<CR>
" Move between bufffers
nnoremap <silent> <Leader>[ :bprev<CR>
nnoremap <silent> <Leader>] :bnext<CR>
" Delete buffer
nnoremap <silent> <Leader>d :bd<CR>
" Move visual lines
nnoremap j gj
nnoremap k gk


""""""""""""""""""""""""""""""
" Visual configurations
""""""""""""""""""""""""""""""
" Line numbers
set number
" Enable cursor to move one char ahead
set virtualedit=onemore
" Smartindent
set smartindent
" Matching brackets
set showmatch
" show folder icons
let g:WebDevIconsUnicodeDecorateFolderNodes = 1
" Use opened buffer instead of recreating
set switchbuf=useopen
" Show currently inputting commands
set showcmd
" Show current input mode
set showmode
" Colorscheme
colorscheme iceberg
" Darkmode
set background=dark
" Use 256 colors
set t_Co=256
" Enable syntax highlight
syntax on
" Disable wrapping
set nowrap


""""""""""""""""""""""""""""""
" Editing related
""""""""""""""""""""""""""""""
" Visualize unseen characters
set list listchars=tab:\▸\-
" Convert tab as multiple spaces
set expandtab
" Other configs
set tabstop=2
set shiftwidth=4
set autoindent
set smartindent
" Enable backspace
set backspace=indent,eol,start
" Set clipboard
set clipboard=unnamed

""""""""""""""""""""""""""""""
" Search related
""""""""""""""""""""""""""""""
" Ignore letter cases when searched with all lower cases
set ignorecase
" Differentiate case when searched including upper cases
set smartcase
" Highlight during input
set incsearch
" Return when reached the EOF
set wrapscan
" Highlight targets
set hlsearch
" Escape to end highlight
nmap <Esc><Esc> :nohlsearch<CR><Esc>


""""""""""""""""""""""""""""""
" Other Controls
""""""""""""""""""""""""""""""
" mouse controll
set mouse=a
" Round shifting using < and > to shiftwidth
set shiftround
" Ignore case on completion
set infercase
" Resize windows
nnoremap <S-Left>  <C-w><<CR>
nnoremap <S-Right> <C-w>><CR>
nnoremap <S-Up>    <C-w>-<CR>
nnoremap <S-Down>  <C-w>+<CR>


"#############################
" Other settings
"#############################
" set file encodings
set fileencodings=utf-8,cp932,euc-jp,sjis
" Do not create backups or swapfiles
set nowritebackup
set nobackup
set noswapfile
" Auto reload file if edited
set autoread
" Disable beep sound
set belloff=all
" json preitifier (JQ)
command! -nargs=? Jq call s:Jq(<f-args>)
function! s:Jq(...)
    if 0 == a:0
        let l:arg = "."
    else
        let l:arg = a:1
    endif
    execute "%! jq \"" . l:arg . "\""
endfunction
