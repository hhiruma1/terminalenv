#!/bin/bash

# Update to the newest git
sudo -E add-apt-repository ppa:git-core/ppa &&
sudo apt update &&
sudo apt install -y git
