#!/bin/bash

# Install required packages
sudo apt update &&
sudo apt install -y ripgrep libfuse2 &&

# Install neovim
sudo apt install snapd &&
sudo snap install --classic nvim &&
echo 'export PATH=/snap/bin:$PATH' >> ~/.profile &&

# Install packer.nvim
git clone --depth 1 https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim

# Execute inside neovim
# 1. :PackerInstall
# 2. :LspInfoInstall (jedi)
# 3. :TSInstall
