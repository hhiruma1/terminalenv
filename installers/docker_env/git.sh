#!/bin/bash

# Update to the newest git
add-apt-repository ppa:git-core/ppa &&
apt update &&
apt install -y git
