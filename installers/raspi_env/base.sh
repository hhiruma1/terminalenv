#!/bin/bash

# Install apt dependencies
sudo -E apt update &&
sudo -E apt install -y software-properties-common unzip git wget gcc g++ fontconfig curl &&
mkdir ~/.zsh &&
mkdir ~/.config &&

# Simple python dependencies
if !(type "python3" > /dev/null 2>&1); then
    sudo -E apt install -y python3 python3-venv &&
    python3 -m venv ~/.venv/debugpy &&
    ~/.venv/debugpy/bin/python -m pip install -y debugpy
fi

# Make symbolic_links
cd ~/terminalenv/installers/raspi_env &&
sh ./symbolic_linker.sh &&

# Install nerdfonts
cd /usr/share/fonts &&
sudo -E wget http://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf &&
sudo -E wget http://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf &&
sudo -E wget http://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf &&
sudo -E wget http://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf &&
fc-cache -f -v &&

# Install exa
sudo apt install exa &&

# Install powerlevel10k
cd ~/.zsh &&
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/.zsh/powerlevel10k &&

# Install zsh
sudo -E apt install -y zsh &&
sudo chsh -s $(which zsh) &&

# Install fzf
git clone https://github.com/junegunn/fzf.git ~/.fzf &&
yes | ~/.fzf/install
