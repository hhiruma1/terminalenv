set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
set shell=sh
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'


" 導入したいプラグインを以下に列挙
" Plugin '[Github Author]/[Github repo]' の形式で記入
" 上記記入後 vimを再起動した「:BundleInstall」
Plugin 'airblade/vim-gitgutter'                       " vc-codeライクにgitでの差分を表示する
Plugin 'tomasr/molokai'                               " color scheme
Plugin 'bronson/vim-trailing-whitespace'              " 末尾の全角と半角の空白文字を赤くハイライト
Plugin 'christoomey/vim-tmux-navigator'               " tmux-vim Pane 移動キーバインドシームレス化
Plugin 'scrooloose/nerdtree'                          " :NERDTree
"Plugin 'tpope/vim-fugitive'                           " 便利なvim plugin
Plugin 'vim-airline/vim-airline'                      " vimのステータスラインを装飾するプラグイン
Plugin 'vim-airline/vim-airline-themes'               " vim-airlineのテーマ
Plugin 'terryma/vim-expand-region'                    " 選択範囲拡大のプラグイン
Plugin 'numirias/semshi'                              " enhanced python syntax highlight
Plugin 'tiagofumo/vim-nerdtree-syntax-highlight'      " NERDTreeのSytax Highlight Plugin
Plugin 'ryanoasis/vim-devicons'                       " deviconが使えるようになる
Plugin 'tsony-tsonev/nerdtree-git-plugin'             " NERDTreeをgit対応させるプラグイン
Plugin 'Shougo/unite.vim'                             " Unite.vim
Plugin 'Shougo/unite-outline'                         " Unite outline
Plugin 'posva/vim-vue'                                " vue 用のsyntax highlight
Plugin 'nathanaelkane/vim-indent-guides'              " インデントを視覚的にわかりやすく
Plugin 'davidhalter/jedi-vim'                         " Python の補完機能
Plugin 'ervandew/supertab'                            " タブキーで補完できるようにする
Plugin 'thinca/vim-quickrun'                          " 非同期実行
Plugin 'jmcantrell/vim-virtualenv'                    " Virtualenv
Plugin 'vim-syntastic/syntastic'                      " Python syntax checker
Plugin 'nvie/vim-flake8'                              " Python syntax checker
Plugin 'tweekmonster/braceless.vim'                   " Python 折り畳み用


call vundle#end()
filetype plugin indent on



"　その他のカスタム設定を以下に書く

"文字コードをUFT-8に設定
"set fenc=utf-8
"ファイルを読み込む時の、文字コード自動判別の順番
set fileencodings=utf-8,cp932,euc-jp,sjis
" バックアップファイルを作らない
set nobackup
" スワップファイルを作らない
set noswapfile
" 編集中のファイルが変更されたら自動で読み直す
set autoread
" バッファが編集中でもその他のファイルを開けるように
set hidden
" 入力中のコマンドをステータスに表示する
set showcmd
" 入力モードをステータスに表示する
set showmode
" ビープ音無効化
set belloff=all
" カラースキーム設定
"colorscheme molokai
colorscheme iceberg
" ダークモード
set background=dark
" 色を256色に変更
set t_Co=256
" syntax highlightをオンにする
syntax on
" 更新タイミングをデフォルトの4000から変える
set signcolumn=yes
" Swapファイル？Backupファイル？前時代的すぎなので全て無効化する
set nowritebackup
set nobackup
set noswapfile
" backspaceを有効化
set backspace=indent,eol,start
" Leader設定
let mapleader = "\<Space>"
" vimの無名レジスタとクリップボードの連携
set clipboard=unnamed
" vue のsytax highlightが途中で切れる防止
autocmd FileType vue syntax sync fromstart
set nowrap
" コードをたたむ
"set foldmethod=indent
"set foldlevel=99
" コードをたたむコマンドをリマップ
"nnoremap z za



""""" Leaderへのマッピング""""""
" .vimrcのリロード
nnoremap <Leader>r :source ~/.vimrc<CR>:noh<CR>
" 保存
nnoremap <Leader>w :w<CR>
" 閉じる
nnoremap <Leader>q :w<CR>
" 画面水平分割
nnoremap <Leader>s :split<CR>
" 画面縦分割
nnoremap <Leader>v :vsplit<CR>
" NERDTreeを表示
nnoremap <silent> <Leader>n :NERDTreeToggle<CR>
" NERDTreeで現在開いているファイルを表示
nnoremap <silent> <Leader>f :NERDTreeFind<CR>
" vimのバッファを切り替え
nnoremap <silent> <Leader>[ :bprev<CR>
nnoremap <silent> <Leader>] :bnext<CR>
" vimのバッファを削除
nnoremap <silent> <Leader>d :bd<CR>
" VundleInstall
nnoremap <silent> <Leader>b :VundleInstall<CR>
"" タブ操作関連
" 新しいタブ作成(tab edit)
nnoremap <silent> <Leader>tt :tabe<CR>
" 次のタブに移動(tab next)
nnoremap <silent> <Leader>tn :tabn<CR>
" 前のタブに移動（tab previous)
nnoremap <silent> <Leader>tN :tabN<CR>
" タブを閉じる (tab close)
nnoremap <silent> <Leader>tc :tabc<CR>
" 今開いているタブ以外を消す (tab only)
nnoremap <silent> <Leader>to :tabo<CR>


"""""" 見た目系 """"""""""
" 行番号を表示
set number
" 現在の行を強調表示
"set cursorline
" 現在の列を強調表示
" set cursorcolumn
" 行末の1文字先までカーソルを移動できるように
set virtualedit=onemore
" インデントはスマートインデント
set smartindent
" ビープ音を可視化
set visualbell
" 括弧入力時の対応する括弧を表示
set showmatch
" ステータスラインを常に表示
set laststatus=2
" ステータスラインにブランチ名を表示
set statusline=%<%f\ %h%m%r%{fugitive#statusline()}%=%-14.(%l,%c%V%)\ \[ENC=%{&fileencoding}]%P
" コマンドラインの補完
set wildmode=list:longest
" 折り返し時に表示行単位での移動できるようにする
nnoremap j gj
nnoremap k gk
" マウス操作用
set mouse=a
set ttymouse=xterm2
" タブページを常に表示
set showtabline=2
" フォルダのアイコンを表示
let g:WebDevIconsUnicodeDecorateFolderNodes = 1



"""""""""""""""""""""""""""""""
" Tab系
"""""""""""""""""""""""""""""""

" 不可視文字を可視化(タブが「▸-」と表示される)
set list listchars=tab:\▸\-
" Tab文字を半角スペースにする
set expandtab
" 行頭以外のTab文字の表示幅（スペースいくつ分）
set tabstop=2
" 行頭でのTab文字の表示幅
set shiftwidth=4
set autoindent
set smartindent


"""""""""""""""""""""""""""""""
" 検索系
"""""""""""""""""""""""""""""""

" 検索文字列が小文字の場合は大文字小文字を区別なく検索する
set ignorecase
" 検索文字列に大文字が含まれている場合は区別して検索する
set smartcase
" 検索文字列入力時に順次対象文字列にヒットさせる
set incsearch
" 検索時に最後まで行ったら最初に戻る
set wrapscan
" 検索語をハイライト表示
set hlsearch
" ESC連打でハイライト解除
nmap <Esc><Esc> :nohlsearch<CR><Esc>




"""""""""""""""""""""""""""""""
" 編集系
"""""""""""""""""""""""""""""""

" '<'や'>'でインデントする際に'shiftwidth'の倍数に丸める
set shiftround
" 補完時に大文字小文字を区別しない
set infercase
" バッファを閉じる代わりに隠す（Undo履歴を残すため）
set hidden
" 新しく開く代わりにすでに開いてあるバッファを開く
set switchbuf=useopen
" 対応する括弧などをハイライト表示する
set showmatch
" 対応括弧のハイライト表示を3秒にする
set matchtime=3

" Shift + 矢印でウィンドウサイズを変更
nnoremap <S-Left>  <C-w><<CR>
nnoremap <S-Right> <C-w>><CR>
nnoremap <S-Up>    <C-w>-<CR>
nnoremap <S-Down>  <C-w>+<CR>

" pythonシンタックスハイライト用
 if version < 600
  syntax clear
elseif exists('b:current_after_syntax')
  finish
endif

" jq(json pretify) 用
command! -nargs=? Jq call s:Jq(<f-args>)
function! s:Jq(...)
    if 0 == a:0
        let l:arg = "."
    else
        let l:arg = a:1
    endif
    execute "%! jq \"" . l:arg . "\""
endfunction

" We need nocompatible mode in order to continue lines with backslashes.
" Original setting will be restored.
let s:cpo_save = &cpo
set cpo&vim

syn match pythonOperator "\(+\|=\|-\|\^\|\*\)"
syn match pythonDelimiter "\(,\|\.\|:\)"
syn keyword pythonSpecialWord self

hi link pythonSpecialWord    Special
hi link pythonDelimiter      Special

let b:current_after_syntax = 'python'

let &cpo = s:cpo_save
unlet s:cpo_save



"""""""""""""""""""""""""""""""
" プラグイン用の設定
"""""""""""""""""""""""""""""""

" alvan/vim-closetag用
" vueファイルにも閉じタグを挿入する
let g:closetag_filenames = '*.html,*.vue'

let g:comfortable_motion_scroll_down_key = "j"
let g:comfortable_motion_scroll_up_key = "k"


" terryma/vim-expand-region用
vmap v <Plug>(expand_region_expand)
vmap <C-v> <Plug>(expand_region_shrink)


" vim-virtualenv用
let g:virtualenv_directory='~/venv'


" tweekmosnster/braceless.vim用
autocmd FileType python BracelessEnable +fold
" 使い方メモ：
" 移動:
"    [[　前のブロックの:
"    ]]　次のブロックの:
"    [m  前のブロックの先頭
"    ]m  次のブロックの先頭
" 折りたたみ:
"    zc  閉じる
"    zC  再帰的に閉じる
"    zo  開く
"    zO  再帰的に開く



" NERDTree用
" 起動時にNERDTree起動
" autocmd vimenter * NERDTree
" NERDTreeでルートを変更したらchdirする
let g:NERDTreeChDirMode = 2


" air-line/air-line用の設定
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamemode = ':t'
let g:airline#extensions#tabline#buffer_idx_mode = 1
let g:airline#extensions#tabline#left_sep = ''
let g:airline#extensions#tabline#left_alt_sep = ''
let g:airline#extensions#tabline#buffer_idx_format = {
  \ '0': '0 ',
  \ '1': '1 ',
  \ '2': '2 ',
  \ '3': '3 ',
  \ '4': '4 ',
  \ '5': '5 ',
  \ '6': '6 ',
  \ '7': '7 ',
  \ '8': '8 ',
  \ '9': '9 '
  \}
let g:airline_theme='iceberg'
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.crypt = '🔒'
let g:airline_symbols.linenr = '☰'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.maxlinenr = '㏑'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.spell = 'Ꞩ'
let g:airline_symbols.notexists = 'Ɇ'
let g:airline_symbols.whitespace = 'Ξ'
" powerline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = '☰'
let g:airline_symbols.maxlinenr = ''



" Jedi vim
"" Settings
let g:jedi#popup_on_dot = 0
"" Commands
let g:jedi#goto_command = "<leader>d"
let g:jedi#goto_assignments_command = ""
let g:jedi#goto_definitions_command = ""
let g:jedi#documentation_command = ""
let g:jedi#usages_command = "<leader>,"
let g:jedi#completions_command = ""
let g:jedi#rename_command = ""
"" Colors
"highlight jediFunction guifg=#f5f5f5 guibg=#4682b4
highlight jediFunction guifg=#ffff00 guibg=#ffff00
"highlight jediFunction guifg=#ffffff guibg=#cc99ff
"highlight jediFat guifg=#f5f5f5 guibg=#4682b4
"highlight jediFat ctermbg=darkblue



" NERDTress File highlighting
let s:brown = "905532"
let s:aqua =  "3AFFDB"
let s:blue = "689FB6"
let s:darkBlue = "44788E"
let s:purple = "834F79"
let s:lightPurple = "834F79"
let s:red = "AE403F"
let s:beige = "F5C06F"
let s:yellow = "F09F17"
let s:orange = "D4843E"
let s:darkOrange = "F16529"
let s:pink = "CB6F6F"
let s:salmon = "EE6E73"
let s:green = "8FAA54"
let s:lightGreen = "31B53E"
let s:white = "FFFFFF"
let s:rspec_red = 'FE405F'
let s:git_orange = 'F54D27'

" python
let g:NERDTreeExtensionHighlightColor = {} " this line is needed to avoid error
let g:NERDTreeExtensionHighlightColor['py'] = s:yellow

" web
let g:NERDTreePatternMatchHighlightColor = {} " this line is needed to avoid error
let g:NERDTreePatternMatchHighlightColor['json'] = s:yellow
let g:NERDTreePatternMatchHighlightColor = {} " this line is needed to avoid error
let g:NERDTreePatternMatchHighlightColor['js'] = s:yellow
let g:NERDTreePatternMatchHighlightColor = {} " this line is needed to avoid error
let g:NERDTreePatternMatchHighlightColor['css'] = s:orange
let g:NERDTreePatternMatchHighlightColor = {} " this line is needed to avoid error
let g:NERDTreePatternMatchHighlightColor['html'] = s:orange
let g:NERDTreePatternMatchHighlightColor = {} " this line is needed to avoid error
let g:NERDTreePatternMatchHighlightColor['php'] = s:purple

" C
let g:NERDTreePatternMatchHighlightColor = {} " this line is needed to avoid error
let g:NERDTreePatternMatchHighlightColor['c'] = s:aqua
let g:NERDTreePatternMatchHighlightColor = {} " this line is needed to avoid error
let g:NERDTreePatternMatchHighlightColor['c++'] = s:aqua

" java
let g:NERDTreePatternMatchHighlightColor = {} " this line is needed to avoid error
let g:NERDTreePatternMatchHighlightColor['java'] = s:red

" ruby
let g:NERDTreePatternMatchHighlightColor = {} " this line is needed to avoid error
let g:NERDTreePatternMatchHighlightColor['ruby'] = s:red

"other
let g:NERDTreeExactMatchHighlightColor = {} " this line is needed to avoid error
let g:NERDTreeExactMatchHighlightColor['.gitignore'] = s:beige " sets the color for .gitignore files
let g:NERDTreePatternMatchHighlightColor = {} " this line is needed to avoid error
let g:NERDTreePatternMatchHighlightColor['config'] = s:beige

let g:NERDTreeGitStatusWithFlags = 1
let g:NERDTreeIndicatorMapCustom = {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ 'Ignored'   : '☒',
    \ "Unknown"   : "?"
    \ }
