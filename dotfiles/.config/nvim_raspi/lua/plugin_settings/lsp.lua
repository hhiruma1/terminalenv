--LSP
local cmp = require('cmp')
local lspconfig = require('lspconfig')
local compare = require('cmp.config.compare')
local luasnip = require('luasnip')

local capabilities = require('cmp_nvim_lsp').default_capabilities()

local source_mapping = {
    buffer = "[Buffer]",
    nvim_lsp = "[LSP]",
    nvim_lua = "[Lua]",
    cmp_tabnine = "[TN]",
    path = "[Path]",
    cmdline = '[CMD]',
    dap = '[DAP]',
    treesitter = '[TS]',
}

vim.opt.completeopt = {'menu', 'menuone', 'noselect'}
cmp.setup {
    sources = {
        {name = 'nvim_lsp'},
        {name = 'path'},
        {name = 'cmdline'},
        {name = 'nvim_lsp_signature_help'},
        {name = 'treesitter'},
        {name = 'cmp_tabnine'},
        {name = 'dap'},
        {name = 'luasnip'},
    },
    window = {
        completion = cmp.config.window.bordered(),
        documentation = cmp.config.window.bordered(),
    },
    mapping = {
        ['<C-p>'] = cmp.mapping.select_prev_item(),
        ['<C-n>'] = cmp.mapping.select_next_item(),
        ['<C-d>'] = cmp.mapping.scroll_docs(-4),
        ['<C-f>'] = cmp.mapping.scroll_docs(4),
        ['<C-e>'] = cmp.mapping.close(),
        ['<CR>'] = cmp.mapping.confirm({ select = true }),
        ['<C-k>'] = cmp.mapping(function(fallback)
            if luasnip.expand_or_jumpable() then
                luasnip.expand_or_jump()
            else
                fallback()
            end
        end, {'i', 's'} )
    },
    formatting = {
    },
    snippet = {
        expand = function(args)
            require'luasnip'.lsp_expand(args.body)
        end
    },
    enabled = function ()
        return vim.api.nvim_buf_get_option(0, 'buftype') ~= 'prompt'
                    or require('cmp_dap').is_dap_buffer()
    end,
    sorting = {
        priority_weight = 2,
        comparators = {
            compare.offset,
            compare.exact,
            compare.score,
            compare.recently_used,
            compare.kind,
            compare.sort_text,
            compare.length,
            compare.order,
        },
    },
}

cmp.setup.cmdline('/', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = {
        { name = 'buffer' }
    }
})

cmp.setup.cmdline(':', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({
        { name = 'path' }
    }, {
        { name = 'cmdline' }
    })
})

lspconfig.jedi_language_server.setup {}

local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
for type, icon in pairs(signs) do
  local hl = "DiagnosticSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end
