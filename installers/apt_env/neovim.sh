#!/bin/bash

# Install required packages
sudo add-apt-repository universe &&
sudo apt update &&
sudo apt install -y ripgrep libfuse2 &&

# Install neovim
cd ~/.zsh &&
wget https://github.com/neovim/neovim/releases/download/stable/nvim-linux-x86_64.appimage &&
chmod u+x ./nvim.appimage &&
echo 'alias nvim="~/.zsh/nvim.appimage"' > ~/.profile &&

# Install packer.nvim
git clone --depth 1 https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim

# Execute inside neovim
# 1. :PackerInstall
# 2. :LspInfoInstall (jedi)
# 3. :TSInstall
