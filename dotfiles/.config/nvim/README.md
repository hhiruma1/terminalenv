# Neovim settings

## Initial setup
1. Install packages with Packer.nvim
    1. Open neovim
    2. Run `:PackerInstall`
2. Install debugpy to your pyenv environment
    1. pip install debugpy

## Other READMEs
* [README_keybindings.md](./README_keybindings.md): All the main keybinding are written here
* [README_packages.md](./README_packages.md): List of neovim packages and their descriptions

