require("todo-comments").setup{
  keywords = {
    FIX = { icon = " ", color = "error", alt = { "FIXME", "BUG", "FIXIT", "ISSUE" }, },
    TODO = { icon = " ", color = "info", alt = { "ADD" } },
    HACK = { icon = " ", color = "warning", alt = { "DEBUG", "TMP" } },
    WARN = { icon = " ", color = "warning", alt = { "WARNING", "XXX" } },
    PERF = { icon = " ", alt = { "OPTIM", "PERFORMANCE", "OPTIMIZE" } },
    NOTE = { icon = " ", color = "hint", alt = { "INFO", "CHECK" } },
  },
}
