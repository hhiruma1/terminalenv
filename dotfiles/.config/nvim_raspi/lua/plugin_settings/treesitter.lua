--treesitter
require('nvim-treesitter.configs').setup {
    ensure_installed = {'python', 'javascript', 'lua', 'markdown', 'markdown_inline'},
    sync_install = false,
    highlight = {enable = true,},
}
