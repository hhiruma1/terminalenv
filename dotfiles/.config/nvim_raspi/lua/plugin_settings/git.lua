require("neogit").setup {
  use_telescope = true,
  telescope_sorter = function()
    return require("telescope").extensions.fzf.native_fzf_sorter()
  end,
  integrations = {
    diffview = true,
  },
  kind="auto"
}

require('git-conflict').setup()

require('gitsigns').setup{
  signs = {
    add          = {hl = 'gitsignsadd'   , text = '│', numhl='gitsignsaddnr'   , linehl='gitsignsaddln'},
    change       = {hl = 'gitsignschange', text = '│', numhl='gitsignschangenr', linehl='gitsignschangeln'},
    delete       = {hl = 'gitsignsdelete', text = '_', numhl='gitsignsdeletenr', linehl='gitsignsdeleteln'},
    topdelete    = {hl = 'gitsignsdelete', text = '‾', numhl='gitsignsdeletenr', linehl='gitsignsdeleteln'},
    changedelete = {hl = 'gitsignschange', text = '~', numhl='gitsignschangenr', linehl='gitsignschangeln'},
  },
  numhl = true,
  linehl = false,
}
