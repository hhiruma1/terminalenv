local mason = require('mason')
local mason_lspconfig = require('mason-lspconfig')
local lspconfig = require('lspconfig')
local capabilities = require('cmp_nvim_lsp').default_capabilities()
local mason_tool_installer = require('mason-tool-installer')

mason.setup({
   ui = {
     border = 'single',
     icons = {
       package_installed = "✓",
       package_pending = "➜",
       package_uninstalled = "✗"
     }
   }
 })

mason_tool_installer.setup {
    ensure_installed = {
        -- LSP
        'jedi_language_server',
        'pyright',
        'lua_ls',
        -- DAP
        'debugpy',
        -- Linters
        'pydocstyle',
    }
}

mason_lspconfig.setup_handlers({
    function(server_name)
        local opts = {}
        opts.settings = {
            on_attach = on_attach,
            capabilities = capabilities,
        }
        lspconfig[server_name].setup(opts)
    end
})

require('mason-nvim-dap').setup({
    ensure_installed = {'python' }
})

mason_lspconfig.setup_handlers({
    function(server_name)
        local opts = {}
        opts.settings = {
            on_attach = on_attach,
            capabilities = capabilities,
        }
        lspconfig[server_name].setup(opts)
    end
})
