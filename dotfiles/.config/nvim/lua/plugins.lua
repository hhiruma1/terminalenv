vim.cmd[[packadd packer.nvim]]

require'packer'.startup(function()
    -- General
    use'wbthomason/packer.nvim'

    -- Visual related
    ---- *nvim-web-devicions: show devicons
    use'kyazdani42/nvim-web-devicons'
    ---- *bufferline: show top bar
    use{'akinsho/bufferline.nvim', tag="*", requires='nvim-tree/nvim-web-devicons'}
    ---- *scope: Add tab control to buffer line
    use("tiagovla/scope.nvim")
    ---- lualine: show bottom bar
    use{'nvim-lualine/lualine.nvim', requires={'kyazdani42/nvim-web-devicons', opt=true}}
    ---- nvim-notify: better visualization of notifications
    use'rcarriga/nvim-notify'
    ---- indent-blankline: show indentation levels with lines
    use'lukas-reineke/indent-blankline.nvim'
    ---- tinit.nvim: Tint inactive windows in Neovim using window-local highlight namespaces.
    use'levouh/tint.nvim'
    ---- noice.nvim: Highly experimental plugin that completely replaces the UI for messages, cmdline and the popupmenu.
    use{'folke/noice.nvim', requires={'MunifTanjim/nui.nvim'}}

    -- Editor
    ---- *vim-better-whitespace: show white space with red blocks
    use'ntpeters/vim-better-whitespace'
    ---- *vim-expand-region: visually select increasingly larger regions of text using the same key combination.
    use'terryma/vim-expand-region'
    ---- *vim-visual-multi: edit multiple positions simultaneously
    use'mg979/vim-visual-multi'
    ---- *nvim-cursorline: underlines the same words the cursor is on
    use'yamatsum/nvim-cursorline'
    ---- *hlargs: highlihgt argument definitions and usages
    use'm-demare/hlargs.nvim'
    ---- nvim-numbertoggle: Automatically switch between absolute/relative line numbers
    use'sitiom/nvim-numbertoggle'
    ---- nvim-autopairs: Auto input a pair of parentheses
    use'windwp/nvim-autopairs'
    ---- copilot.vim: Github copilot (need to set proxy settings and execute :Copilot setup)
    use'github/copilot.vim'

    -- Search
    ---- nvim-hlslens: show position of search
    use'kevinhwang91/nvim-hlslens'
    ---- pounce.nvim: incremental search
    use'rlane/pounce.nvim'
    ---- goto-preview: jump to definitions
    use{
        'rmagatti/goto-preview',
        config = function()
            require('goto-preview').setup {}
        end
    }

    -- Filer
    ---- *neo-tree: show file tree view
    use{'nvim-neo-tree/neo-tree.nvim',
        branch='v2.x',
        requires={'nvim-lua/plenary.nvim', 'kyazdani42/nvim-web-devicons', 'MunifTanjim/nui.nvim' }
    }
    ---- *telescope: fuzzy finders
    use{'nvim-telescope/telescope.nvim', requires='nvim-lua/plenary.nvim'}
    use'nvim-telescope/telescope-media-files.nvim'
    use'nvim-telescope/telescope-fzy-native.nvim'

    -- Movement
    ---- *vim-tmux-navigator: Seamless navigation between vim and tmux
    use'christoomey/vim-tmux-navigator'
    ---- leap.nvim: A general-purpose motion plugin for Neovim
    use{'ggandor/leap.nvim', requires='tpope/vim-repeat'}
    ---- before.nvim: Track edit locations and jump back to them
    use'bloznelis/before.nvim'

    -- Colorscheme
    ---- *nightfox: colorscheme
    use'EdenEast/nightfox.nvim'
    ---- tokyonight: colorscheme (TODO: toggle between tokyo-storm and tokyo-night)
    use'folke/tokyonight.nvim'

    -- Visuals
    ---- nvim-scrollbar: simple visualization of scrollbar
    use'petertriho/nvim-scrollbar'
    ---- mode: change line color based on mode
    use{'mvllow/modes.nvim', tag='v0.2.0',
        config = function()
            require('modes').setup()
        end
    }

    -- Comments
    ---- Comment: toggle comment/uncomment state
    use'numToStr/Comment.nvim'
    ---- todo-comments: highlight certain keywords with different colors
    use{'folke/todo-comments.nvim', requires='nvim-lua/plenary.nvim', opts={}}

    -- Mason
    ---- *Mason: package manager for neovim (lsp servers, dap servers, linters, formatters
    use'williamboman/mason.nvim'
    ---- *Mason-lsp-config: manage link with lspconfig
    use'williamboman/mason-lspconfig.nvim'
    ---- *Mason-nvim-dap:  bridges mason.nvim with the nvim-dap plugin
    use'jay-babu/mason-nvim-dap.nvim'
    ---- *Mason-tool-installer:  Autoinstall mason packages
    use'WhoIsSethDaniel/mason-tool-installer.nvim'

    -- LSP
    ---- *nvim-cmp: A completion engine plugin for neovim
    use'hrsh7th/nvim-cmp'
    ---- *nvim-lspconfig: A suite of config files for LSP
    use'neovim/nvim-lspconfig'
    ---- *cmp-nvim-lsp: nvim-cmp source for neovim's built-in language server client
    use'hrsh7th/cmp-nvim-lsp'
    ---- cmp-nvim-lsp-signature-help: nvim-cmp source for displaying function signatures with the current parameter emphasized
    use'hrsh7th/cmp-nvim-lsp-signature-help'
    ---- cmp-cmdline: nvim-cmp source for vim's cmdline.
    use'hrsh7th/cmp-cmdline'
    ---- cmp-path: nvim-cmp source for path
    use'hrsh7th/cmp-path'
    ---- cmp-treesitter: nvim-cmp source from treesitter
    use'ray-x/cmp-treesitter'
    ---- lspkind: show vscode-like pictograms in lsp
    use'onsails/lspkind.nvim'
    ---- nvim-cmp: source for nvim-dap REPL and nvim-dap-ui buffers
    use'rcarriga/cmp-dap'
    ---- cmp-tabnine: tabnine source for nvim-cmp
    -- use{'tzachar/cmp-tabnine', run='./install.sh', requires='hrsh7th/nvim-cmp'}
    ---- cmp_luasnip: luasnip completion source for nvim-cmp
    use'saadparwaiz1/cmp_luasnip'

    -- LSP plugins
    ---- *lspsaga: improved lsp UI
    -- https://zenn.dev/botamotch/articles/c02c51cff7d61d
    use{'nvimdev/lspsaga.nvim',
        config = function()
            require('lspsaga').setup({})
        end,
    }
    ---- *treesitter: linter
    use'nvim-treesitter/nvim-treesitter'
    ---- *lsp-colors: coloring diagnostics
    use'folke/lsp-colors.nvim'
    ---- *trouble: pretty list for showing diagnostics, references, telescope results
    use{'folke/trouble.nvim', requires='kyazdani42/nvim-web-devicons'}
    ---- lsp_lines: Better visualization of diagnosis
    use{"https://git.sr.ht/~whynothugo/lsp_lines.nvim",
        config = function()
            require("lsp_lines").setup()
        end,
    }

    -- Git
    ---- *neogit: Git controll inside neovim
    use{'TimUntersberger/neogit', requires='nvim-lua/plenary.nvim'}
    ---- *diffview: Git diff/merge/history visualization
    use{'sindrets/diffview.nvim', requires='nvim-lua/plenary.nvim'}
    ---- *git-conflict: Highlight conflict areas
    use'akinsho/git-conflict.nvim'
    ---- *gitsigns: Show git decorations
    use'lewis6991/gitsigns.nvim'

    -- DAP (Debug Adapter Protocol)
    -- nvim-dap: DAP client for neovim
    use'mfussenegger/nvim-dap'
    -- nvim-dap-python: DAP for python
    use 'mfussenegger/nvim-dap-python'
    -- nvim-dap-ui: DAP UI neovim
    use{'rcarriga/nvim-dap-ui', requires={'mfussenegger/nvim-dap', 'nvim-neotest/nvim-nio'}}
    -- nvim-dap-virtual-text: Show virtual text during DAP
    use'theHamsta/nvim-dap-virtual-text'

    -- Snippets
    use{'L3MON4D3/LuaSnip', tag='v2.0.0', run='make install_jsregexp'}

    -- Terminal
    use{'akinsho/toggleterm.nvim', tag='*',
        config=function()
            require('toggleterm').setup()
        end
    }

    -- Archive
    ---- flirt: Animations + Resizing + Moving floating windows
    ---- REASON: Not very practical
    -- use'tamton-aquib/flirt.nvim'

    ---- none-ls: Language server to inject LSP diagnostics, code actions, and more via Lua
    ---- REASON: Mason was enough
    -- use{'nvimtools/none-ls.nvim', requires='nvim-lua/plenary.nvim'}

    ---- which-key: Show keymap candidates
    ---- REASON: Never used it
    -- use'folke/which-key.nvim'

    ---- marks.nvim: A better user experience for interacting with and manipulating Vim marks.
    ---- REASON: Never became a habit (preferred leap)
    -- use'chentoast/marks.nvim'
end)
