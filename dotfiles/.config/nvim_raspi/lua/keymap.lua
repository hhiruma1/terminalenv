-----------------------
-- Key Mapping
-----------------------
local keymap = vim.api.nvim_set_keymap

--Leader
vim.g.mapleader = ' '

--Split view
keymap('n', '<Leader>s', '<cmd>split<CR>', {noremap=true})
keymap('n', '<Leader>v', '<cmd>vsplit<CR>', {noremap=true})

--Toggle NERDTree
keymap('n', '<Leader>n', '<cmd>Neotree toggle<CR>', {noremap=true, silent=true})

--Move between buffers
keymap('n', '<Leader>[', '<cmd>bprev<CR>', {noremap=true, silent=true})
keymap('n', '<Leader>]', '<cmd>bnext<CR>', {noremap=true, silent=true})

--Delete buffer
keymap('n', '<Leader>dd', '<cmd>bd<CR>', {noremap=true, silent=true})
keymap('n', '<Leader>DD', '<cmd>BufferLineCloseOthers<CR>', {noremap=true, silent=true})

--Move visual lines
keymap('n', 'j', 'gj', {noremap=true})
keymap('n', 'k', 'gk', {noremap=true})

--Expand visual region
keymap('v', 'v', '<Plug>(expand_region_expand)', {noremap=true})
keymap('v', 'V', '<Plug>(expand_region_shrink)', {noremap=true})

--Resize windows
keymap('n', '<Leader>w', '<C-w>=', {noremap=true})

--Telescope
keymap('n', '<Leader>ff', '<cmd>Telescope find_files prompt_prefix=🔍 <cr>', {noremap=true})
keymap('n', '<Leader>fg', '<cmd>Telescope live_grep prompt_prefix=🔍 <cr>', {noremap=true})
keymap('n', '<Leader>fb', '<cmd>Telescope buffers prompt_prefix=🔍 <cr>', {noremap=true})
keymap('n', '<Leader>fh', '<cmd>Telescope help_tags prompt_prefix=🔍 <cr>', {noremap=true})
keymap('n', '<Leader>fr', '<cmd>Telescope lsp_references prompt_prefix=🔍 <cr>', {noremap=true})
keymap('n', '<Leader>fd', '<cmd>Telescope find_files cwd=~/terminalenv/dotfiles hidden=true <cr>', {noremap=true})
keymap('n', '<Leader>fet', '<cmd>Telescope find_files cwd=~/envtips <cr>', {noremap=true})
keymap('n', '<Leader>fT', '<cmd>TodoTelescope <cr>', {noremap=true})
keymap('n', '<Leader>ft', '<cmd>lua require("telescope-toggleterm").open()<cr>', {noremap=true})
keymap('n', '<Leader>fn', '<cmd>lua require("telescope").extensions.notify.notify()<cr>', {noremap=true})

--LSP
keymap('n', 'K', '<cmd>Lspsaga hover_doc<CR>', {noremap=true})
keymap('n', 'e]', '<cmd>Lspsaga diagnostic_jump_next<CR>', {noremap=true})
keymap('n', 'e[', '<cmd>Lspsaga diagnostic_jump_prev<CR>', {noremap=true})
keymap("n", "<LEADER>jd", "<cmd>Lspsaga peek_definition<CR>", {noremap=true})
keymap("n", "<LEADER>jp", "<cmd>Lspsaga peek_type_definition<CR>", {noremap=true})
keymap("n", "<LEADER>jD", "<cmd>Lspsaga goto_definition<CR>", {noremap=true})

--Git
keymap('n', '<leader>gg', '<cmd>Neogit<cr>', {noremap=true})
keymap('n', '<leader>gdd', '<cmd>diffviewopen<cr>', {noremap=true})
keymap('n', '<leader>gdh', '<cmd>DiffviewFileHistory<cr>', {noremap=true})
keymap('n', '<leader>gsl', '<cmd>gitsigns.toggle_linehl<cr>', {noremap=true})
keymap('n', '<leader>gsw', '<cmd>gitsigns.toggle_word_diff<cr>', {noremap=true})
keymap('n', '<leader>gsd', '<cmd>gitsigns.toggle_deleted<cr>', {noremap=true})

--DAP
keymap('n', '<leader>du', '<cmd>lua require"dapui".toggle()<cr>', {noremap=true})
keymap('n', '<leader>c', '<cmd>lua require"dap".continue()<cr>', {noremap=true})
keymap('n', '<leader>dl', '<cmd>lua require"dap.ext.vscode".load_launchjs("dap_launch.json")<cr>', {noremap=true})
keymap('n', '<leader>db', '<cmd>lua require"dap".toggle_breakpoint()<cr>', {noremap=true})
keymap('n', '<leader>do', '<cmd>lua require"dap".step_over()<cr>', {noremap=true})
keymap('n', '<leader>dr', '<cmd>lua require"dap".step_back()<cr>', {noremap=true})
keymap('n', '<leader>dt', '<cmd>lua require"dap".terminate()<cr>', {noremap=true})

--Terminal
keymap('n', '<leader>tt', '<cmd>ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('n', '<leader>th', '<cmd>ToggleTerm direction=horizontal<cr>', {noremap=true})
keymap('n', '<leader>tf', '<cmd>ToggleTerm direction=float<cr>', {noremap=true})
keymap('n', '<leader>tr', '<cmd>ToggleTermSetName<cr>', {noremap=true})
keymap('n', '<leader>ts', '<cmd>TermSelect<cr>', {noremap=true})
keymap('n', '<leader>1', '<cmd>1ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('n', '<leader>2', '<cmd>2ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('n', '<leader>3', '<cmd>3ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('n', '<leader>4', '<cmd>4ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('n', '<leader>5', '<cmd>5ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('n', '<leader>6', '<cmd>6ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('n', '<leader>7', '<cmd>7ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('n', '<leader>8', '<cmd>8ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('n', '<leader>9', '<cmd>9ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('n', '<leader>0', '<cmd>0ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('t', '<C-h>', '<C-\\><C-n><C-w><C-h>', {noremap=true})
keymap('t', '<C-j>', '<C-\\><C-n><C-w><C-j>', {noremap=true})
keymap('t', '<C-k>', '<C-\\><C-n><C-w><C-k>', {noremap=true})
keymap('t', '<C-l>', '<C-\\><C-n><C-w><C-l>', {noremap=true})
keymap('t', '<A-[>', '<C-\\><C-n>', {noremap=true})
