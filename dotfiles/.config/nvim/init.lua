require 'plugins'
require 'keymap'
require 'colors'
require 'visuals'
require 'basics'

require 'plugin_settings/Comment'
require 'plugin_settings/todo_comments'
require 'plugin_settings/neotree'
require 'plugin_settings/hlslens'
require 'plugin_settings/lsp'
require 'plugin_settings/mason'
require 'plugin_settings/lspsaga'
require 'plugin_settings/treesitter'
require 'plugin_settings/git'
require 'plugin_settings/notify'
require 'plugin_settings/dap'
require 'plugin_settings/scrollbar'
require 'plugin_settings/luasnip'
require 'plugin_settings/toggleterm'
require 'plugin_settings/misc'

--#####################
-- Packer install settings
--#####################
local packer_install_path = '~/.local/share/nvim/site/pack/packer/start/packer.nvim'

if vim.fn.empty(vim.fn.glob(packer_install_path)) > 0 then
  vim.fn.execute(
    '!git clone https://github.com/wbthomason/packer.nvim ' .. packer_install_path
  )
end
