# Key bindings
- LEADER: `<Space>`

- Window realted
    | Category | Command | Description |
    | --- | --- | --- |
    | Movement | `<C-h>` | To left window |
    | Movement | `<C-j>` | To top window |
    | Movement | `<C-k>` | To bottom window |
    | Movement | `<C-l>` | To right window |
    | Split | `<LEADER>s` | Split horizontally |
    | Split | `<LEADER>v` | Split vertically |
    | Buffer | `<LEADER>[` | Go to next buffer |
    | Buffer | `<LEADER>]` | Go to previous buffer |
    | Buffer | `<LEADER>d` | Delete buffer |
    | Other | `<LEADER>w` | Resize all windows |

- Sidebar realted
    | Category | Command | Description |
    | --- | --- | --- |
    | Show | `<LEADER>n` | Toggle Neotree |
    | Show | `<LEADER>b` | Toggle Sidebar |
    | Show | `<LEADER>o` | Show outline |
    | Outline | `<LEADER>{` | Jump to next outline element |
    | Outline | `<LEADER>}` | Jump to previous outline element |

- Edit related
    | Category | Command | Description |
    | --- | --- | --- |
    | Comment | `<LEADER>/` | Comment out linewize (VISUAL mode) |
    | Comment | `<LEADER>?` | Comment out blockwize (VISUAL mode) |
    | Search | `*` | Search the next word the cursor is on (NORMAL mode) |
    | Search | `#` | Search the previous word the cursor is on (NORMAL mode) |

- Fuzzy commands
    | Category | Command | Description |
    | --- | --- | --- |
    | Telescope | `<LEADER>ff` | Find files |
    | Telescope | `<LEADER>fg` | Live grep |
    | Telescope | `<LEADER>fb` | Find buffers |
    | Telescope | `<LEADER>fh` | Find command tags |

- LSP commands
    | Category | Command | Description |
    | --- | --- | --- |
    | LSP | `K` | Show description of variable in hover window
    | LSP | `<LEADER>jd` | Jump to definition
    | LSP | `<LEADER>jr` | Jump to referred lines
    | LSP | `<LEADER>rn` | Rename variables
    | LSP | `<LEADER>aa` | Show code actions
    | LSP | `<LEADER>xx` | Toggle Trouble window
    | goto-preview | `<LEADER>jD` | Jump to definition (floating window)
    | goto-preview | `<LEADER>jF` | Jump to referred lines (floating window)
    | goto-preview | `<LEADER>jx` | Close all floating windows

- Git commands
    | Category | Command | Description |
    | --- | --- | --- |
    | neogit | `<LEADER>g` | Toggle Neogit window
    | neogit | `<LEADER>gd` | Toggle Diffview window
    | fugitive | `:Gdiff` | Show git diff in a split window
    | fugitive | `:Git blame` | Show git blame
    | open-browser-github.vim | `:OpenGithubFile` | Open file in github
    | open-browser-github.vim | `:OpenGithubProject` | Open file in github

- Debug commands
    | Category | Command | Description |
    | --- | --- | --- |
    | DAP | `<LEADER>c` | Start DAP debug
    | DAP | `<LEADER>gb` | Set DAP breakpoint
    | DAP | `<LEADER>go` | Step over
    | DAP | `<LEADER>gr` | Step back
    | DAP | `<LEADER>gu` | Step up the stack
    | DAP | `<LEADER>gd` | Step down the stack
    | DAP | `<LEADER>gt` | Terminate dap
    | DAP | `<LEADER>gg` | Toggle DAP UI
    | DAP | `<LEADER>gf` | Show REPL of DAP UI in floating window
    | DAP | `<LEADER>gl` | Load ./dap_launch.json as launch file
    | DAP | `<LEADER>sr` | SinpRun
    | DAP | `<LEADER>sc` | SinpClose
    

- Terminal commands
    | Category | Command | Description |
    | --- | --- | --- |
    | toggleterm | `<LEADER>tt`: Toggle terminal (vertical)
    | toggleterm | `<LEADER>tv`: Toggle terminal (vertical)
    | toggleterm | `<LEADER>th`: Toggle terminal (horizontal)
    | toggleterm | `<LEADER>tf`: Toggle terminal (float)
