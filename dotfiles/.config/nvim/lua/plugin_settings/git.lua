require("neogit").setup {
  use_telescope = true,
  telescope_sorter = function()
    return require("telescope").extensions.fzf.native_fzf_sorter()
  end,
  integrations = {
    diffview = true,
  },
  kind="auto"
}

require('git-conflict').setup()

require('gitsigns').setup{
  numhl = true,
  linehl = false,
}
