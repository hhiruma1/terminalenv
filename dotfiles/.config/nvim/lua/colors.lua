require('tint').setup()

vim.cmd[[colorscheme nightfox]]
vim.g.lightline = { colorscheme = 'nightfox' }
