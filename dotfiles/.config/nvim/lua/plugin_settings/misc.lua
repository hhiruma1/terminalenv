require('leap').add_default_mappings()
require('before').setup()

require('nvim-autopairs').setup {}

vim.o.timeout = true
vim.o.timeoutlen = 300
