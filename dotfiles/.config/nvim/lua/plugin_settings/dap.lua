require('dapui').setup{
    mappings = {
        edit='e', --edit value of variable
        repl='r', --send variable to repl
    },
}

vim.g.dap_virtual_text = true

require('nvim-dap-virtual-text').setup{
    commented = true,
}
require('dap.ext.vscode').load_launchjs('dap_launch.json')
require('dap-python').setup('~/.pyenv/shims/python')
require('dap.ext.vscode').load_launchjs()

vim.fn.sign_define('DapBreakpoint', {text='⛔', texthl='', linehl='', numhl=''})
vim.fn.sign_define('DapStopped', {text='👉', texthl='', linehl='', numhl=''})
