#zmodload zsh/zprof && zprof

# For nvm
PATH=${NVM_DIR:-$HOME/.config/nvm}/default/bin:$PATH
MANPATH=${NVM_DIR:-$HOME/.config/nvm}/default/share/man:$MANPATH
export NODE_PATH=${NVM_DIR:-$HOME/.config/nvm}/default/lib/node_modules
NODE_PATH=${NODE_PATH:A}
