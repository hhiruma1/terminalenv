ln -sf ~/terminalenv/dotfiles/.zprofile ~/.zprofile
ln -sf ~/terminalenv/dotfiles/.zshrc ~/.zshrc
ln -sf ~/terminalenv/dotfiles/.config/nvim_raspi ~/.config/nvim
ln -sf ~/terminalenv/dotfiles/.tmux.conf ~/.tmux.conf
ln -sf ~/terminalenv/dotfiles/.p10k.zsh ~/.p10k.zsh
