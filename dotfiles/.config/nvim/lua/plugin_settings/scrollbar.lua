--scroll
local colors = require("tokyonight.colors").setup()
require("scrollbar").setup{
    handle = {
        color = colors.bg_highlight,
    },
    marks = {
        search = { color = colors.orange },
        error = { color = colors.error },
        warn = { color = colors.warning },
        info = { color = colors.info },
        hint = { color = colors.hint },
        misc = { color = colors.purple },
    }
}
require("scrollbar.handlers.search").setup()
