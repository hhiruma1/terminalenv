require 'plugins'
require 'keymap'
require 'visuals'
require 'basics'

require 'plugin_settings/Comment'
require 'plugin_settings/todo_comments'
require 'plugin_settings/neotree'
require 'plugin_settings/lsp'
require 'plugin_settings/mason'
require 'plugin_settings/treesitter'
require 'plugin_settings/git'
require 'plugin_settings/luasnip'
require 'plugin_settings/toggleterm'

--#####################
-- Packer install settings
--#####################
local packer_install_path = '~/.local/share/nvim/site/pack/packer/start/packer.nvim'

if vim.fn.empty(vim.fn.glob(packer_install_path)) > 0 then
  vim.fn.execute(
    '!git clone https://github.com/wbthomason/packer.nvim ' .. packer_install_path
  )
end
