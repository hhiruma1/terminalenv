local ls = require('luasnip')
local snip = ls.snippet
local text = ls.text_node
local insert = ls.insert_node

ls.add_snippets(nil, {
    python = {
        snip({
            trig = 'exit',
        }, {
            text({'import sys', 'sys.exit()'}),
            insert(0),
        }),
        snip({
            trig = 'ipdb',
        }, {
            text({'import ipdb', 'ipdb.set_trace()'}),
            insert(0),
        }),
        snip({
            trig = 'np',
        }, {
            text({'import numpy as np'}),
            insert(0),
        }),
        snip({
            trig = 'plt',
        }, {
            text({'import matplotlib.pyplot as plt'}),
            insert(0),
        }),
        snip({
            trig = 'torch',
        }, {
            text({'import torch', 'import torch.nn as nn', 'import torch.nn.functional as F'}),
            insert(0),
        }),
        snip({
            trig = 'device',
        }, {
            text({"device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')"}),
            insert(0),
        }),
        snip({
            trig = 'anim',
        }, {
            text({
                'import matplotlib.animation as anim',
                '',
                'fig, ax = plt.subplots(1, 1)',
                'def anim_update(i):',
                '    ...',
                '',
                'ani = anim.FuncAnimation(fig, anim_update, interval=100, frames=imgs.shape[0])',
                'ani.save("")',
            }),
            insert(0),
        }),
        snip({
            trig = 'args',
        }, {
            text({'parser = argparse.ArgumentParser()', 'parser.add_argument()', 'args = parser.parse_args()'}),
            insert(0),
        }),
        snip({
            trig = 'main',
        }, {
            text({'def main():', '', "if __name__ == '__main__':",}),
            insert(0),
        }),
        snip({
            trig = 'csv',
        }, {
            text({"with open('data/src/sample.csv') as f:", 'reader = csv.reader(f)', 'l = [row for row in reader]',}),
            insert(0),
        }),
        snip({
            trig = '3dplot',
        }, {
            text({
                "fig, ax = plt.subplots(figsize=(6, 6), subplot_kw={'projection': '3d'})",
            }),
            insert(0),
        }),
    },
})
