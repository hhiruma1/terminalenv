require("lspsaga").setup {
  border_style = "single",
  symbol_in_winbar = {
    enable = true,
  },
  lightbulb = {
    enable = false
  },
  show_outline = {
    win_width = 50,
    auto_preview = false,
  },
}
