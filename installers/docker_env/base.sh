#!/bin/bash

# Install apt dependencies
apt update &&
apt install -y software-properties-common unzip git wget gcc g++ fontconfig curl &&
mkdir ~/.zsh &&
mkdir ~/.config &&

# Simple python dependencies
if !(type "python3" > /dev/null 2>&1); then
    apt install -y python3 python3-venv &&
    python3 -m venv ~/.venv/debugpy &&
    ~/.venv/debugpy/bin/python -m pip install -y debugpy
fi

# Make symbolic_links
cd ~/terminalenv &&
sh ./symbolic_linker.sh &&

# Install nerdfonts
cd /usr/share/fonts &&
wget http://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf &&
wget http://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf &&
wget http://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf &&
wget http://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf &&
fc-cache -f -v &&

# Install exa
mkdir ~/.zsh/exa_dir &&
cd ~/.zsh/exa_dir &&
wget https://github.com/ogham/exa/releases/download/v0.10.1/exa-linux-x86_64-v0.10.1.zip &&
unzip exa-linux-x86_64-v0.10.1.zip &&
cp ~/.zsh/exa_dir/bin/exa /usr/local/bin &&

# Install vivid
cd ~/.zsh &&
wget "https://github.com/sharkdp/vivid/releases/download/v0.8.0/vivid_0.8.0_amd64.deb" &&
dpkg -i vivid_0.8.0_amd64.deb &&

# Install powerlevel10k
cd ~/.zsh &&
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/.zsh/powerlevel10k &&

# Install zsh
apt install -y zsh &&
chsh -s $(which zsh) &&

# Install fzf
git clone https://github.com/junegunn/fzf.git ~/.fzf &&
yes | ~/.fzf/install
