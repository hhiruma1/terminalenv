source ~/.profile

# -----------------------------
# # powerlevel10k
# -----------------------------
# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi
source  ~/.zsh/powerlevel10k/powerlevel10k.zsh-theme

# -----------------------------
# # PATH
# -----------------------------
# neovim
export XDG_CONFIG_HOME=~/.config
export XDG_CACHE_HOME=~/.cache

# ------------------------------------------
# その他の環境変数
# ------------------------------------------
export EDITOR=vim
export CUBLAS_WORKSPACE_CONFIG=:4096:8
alias cl='clear'
alias li='litecli $1'
alias d='cd ../'
alias h='cd ~'
alias gs='git status'
alias ta='tmux attach-session -t $1'
alias tl='tmux ls'
alias dkst='sudo service docker start'

# --------------------------------------------------
#  $ cd-bookmark
# --------------------------------------------------
fpath=($HOME/zsh/functions/cd-bookmark(N-/) $fpath)
autoload -Uz cd-bookmark
alias b='cd-bookmark'

# --------------------------------------------------
#  gitコマンド補完機能セット
# --------------------------------------------------
# autoloadの文より前に記述
fpath=(~/.zsh/completion $fpath)

# --------------------------------------------------
#  コマンド入力補完
# --------------------------------------------------
# 補完候補に色つける
autoload -U colors
colors
zstyle ':completion:*' list-colors "${LS_COLORS}"

# 単語の入力途中でもTab補完を有効化
setopt complete_in_word
# 補完候補をハイライト
zstyle ':completion:*:default' menu select=1
# キャッシュの利用による補完の高速化
zstyle ':completion::complete:*' use-cache true
# 大文字、小文字を区別せず補完する
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
# 補完リストの表示間隔を狭くする
setopt list_packed

# docker系
zstyle ':completion:*:*:docker:*' option-stacking yes
zstyle ':completion:*:*:docker-*:*' option-stacking yes

# コマンドの打ち間違いを指摘してくれる
setopt correct
SPROMPT="correct: $RED%R$DEFAULT -> $GREEN%r$DEFAULT ? [Yes/No/Abort/Edit] => "

# コマンド履歴
HISTFILE=~/.zsh_history
HISTSIZE=6000000
SAVEHIST=6000000
autoload history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end
bindkey "^P" history-beginning-search-backward-end
bindkey "^N" history-beginning-search-forward-end

# --------------------------------------------------
#  $ cd 機能拡張
# --------------------------------------------------
# cdを使わずにディレクトリを移動できる
setopt auto_cd
# $ cd - でTabを押すと、ディレクトリの履歴が見れる
setopt auto_pushd


# --------------------------------------------------
#  $ lsに色を付けたモード
# --------------------------------------------------
export LS_COLORS="$(vivid generate nord)"
if [[ $(command -v exa) ]]; then
  alias l='exa --icons -Flar -s=type --git --group-directories-first'
  alias ls='exa --icons -FGar -s=type --group-directories-first'
else
  alias l='ls -la'
  alias ls='ls'
fi
export LS_COLORS="di=33:fi=32:[^\.]*=37"

# --------------------------------------------------
#  $ tree でディレクトリ構成表示
# --------------------------------------------------
alias tree="pwd;find . | sort | sed '1d;s/^\.//;s/\/\([^/]*\)$/|--\1/;s/\/[^/|]*/| /g'"

# --------------------------------------------------
#  $ vim のキーバインドを反映
# --------------------------------------------------
bindkey -v

# --------------------------------------------------
#  $ ビープ音無効化
# --------------------------------------------------
setopt no_beep

# --------------------------------------------------
#  $ viモードを表示する
# --------------------------------------------------
# 表示切り替えの遅延をなくす
KEYTIMEOUT=5

# viモード切り替え時に呼ばれる
zle-keymap-select () {
    case $KEYMAP in
        vicmd)      print -n -- "\033[2 q";;  # ノーマルモードはブロックカーソル
        viins|main) print -n -- "\033[6 q";;  # 挿入モードはラインカーソル
    esac
}

# 初期化時 (プロンプト表示時の)
zle-line-init () {
  zle -K viins # 初期化時は挿入モード
  printf "\033[6 q"
}

# 終了時　（vimなどアプリ並行する時)
# !!!!!!!!注意!!!!!!!!!!!
# 　この部分を消すと以下のことが起きるので絶対消さないように
#       ラインカーソル（挿入モード）のままvimを開くとカーソルの設定がそのままになって
#       vim内でもラインカーソルになる
zle-line-finish () {
  zle -K vicmd # vimへ行く時はノーマルモードにする
  printf "\033[2 q"
}

function zle-line-init zle-keymap-select {
  vimode="${${KEYMAP/vicmd/NORMAL}/(main|viins)/INSERT}"
}

zle -N zle-line-init
zle -N zle-keymap-select

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh


############################
# Plugin manager
############################
### Added by Zinit's installer
if [[ ! -f $HOME/.local/share/zinit/zinit.git/zinit.zsh ]]; then
    print -P "%F{33} %F{220}Installing %F{33}ZDHARMA-CONTINUUM%F{220} Initiative Plugin Manager (%F{33}zdharma-continuum/zinit%F{220})…%f"
    command mkdir -p "$HOME/.local/share/zinit" && command chmod g-rwX "$HOME/.local/share/zinit"
    command git clone https://github.com/zdharma-continuum/zinit "$HOME/.local/share/zinit/zinit.git" && \
        print -P "%F{33} %F{34}Installation successful.%f%b" || \
        print -P "%F{160} The clone has failed.%f%b"
fi

source "$HOME/.local/share/zinit/zinit.git/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

# Load a few important annexes, without Turbo
# (this is currently required for annexes)
zinit light-mode for \
    zdharma-continuum/zinit-annex-as-monitor \
    zdharma-continuum/zinit-annex-bin-gem-node \
    zdharma-continuum/zinit-annex-patch-dl \
    zdharma-continuum/zinit-annex-rust

### End of Zinit's installer chunk


############################
# Add plugins
############################
zplugin light "zsh-users/zsh-syntax-highlighting"
zplugin light "zsh-users/zsh-autosuggestions"
zplugin light "zsh-users/zsh-completions"

zplugin ice wait'!0'; zplugin light "b4b4r07/enhancd"

############################
# Settings for plugins
############################
# zsh-users/zsh-autosuggestions
bindkey '^f' autosuggest-accept
bindkey '^g' autosuggest-execute

# fzf
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export FZF_DEFAULT_OPTS='--height 40% --reverse --border'

# fd - cd to selected directory
fd() {
  local dir
  dir=$(find ${1:-.} -path '*/\.*' -prune \
                  -o -type d -print 2> /dev/null | fzf +m) &&
  cd "$dir"
}

# nvm (nodejs version manager)
nvm() {
    unset -f nvm
    source "${NVM_DIR:-$HOME/.config/nvm}/nvm.sh"
    nvm "$@"
}
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
