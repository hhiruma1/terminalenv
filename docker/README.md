# Docker EXEC + TMUX = Break?
Running tmux inside docker components seems to break the visuals.
This can be avoided by connecting to components via ssh instead of
using docker exec.

## Instructions
1. Select your base docker image
2. Use dockerfile and create a new image with openssh-server installed.
    - Make sure to change your password
3. Build your new docker image:
    - `docker build -t test .`
4. Create new container with detached mode
    - `docker run -d -P --name [image_name] [container_name]`
5. Connect to container via ssh
    - `ssh root@localhost -p $(docker port [container_name] 22|sed 's/0.0.0.0:.*//g'|sed 's/::://g')`
