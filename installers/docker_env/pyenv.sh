#!/bin/bash

# Install dependencies
apt install -y python3-tk zlib1g-dev libssl-dev &&
apt install -y liblzma-dev libsqlite3-dev &&

# Clone pyenv
if [ ! -d "~/.pyenv" ]; then
  yes | git clone https://github.com/pyenv/pyenv.git ~/.pyenv
fi

# Install python
export PYENV_ROOT="$HOME/.pyenv" &&
export PATH="$PYENV_ROOT/bin:$PATH" &&
export PATH="$PYENV_ROOT/shims:$PATH" &&
pyenv install 3.10.10 &&
pyenv global 3.10.10 &&

# Install pyenv-virtualenv
git clone https://github.com/yyuu/pyenv-virtualenv.git ~/.pyenv/plugins/pyenv-virtualenv
