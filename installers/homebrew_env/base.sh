#!/bin/bash

# Before running this script ...
#  1. Install iTerm2 (for good visual outputs)
#  2. Go to Preferences -> Security and Privacy -> Full disk access and add iTerm2 to it

# Install homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)" &&
brew update &&

# Create basic directories
mkdir ~/.zsh &&

# Install basic packages
brew install wget fontconfig &&

# Install latest git
brew install git &&
echo 'export PATH="/opt/homebrew/bin:$PATH"' >> ~/.profile &&
source ~/.profile &&

# Make symbolic_links
cd ~/terminalenv &&
sh ./symbolic_linker.sh &&

# Install nerdfont
brew tap homebrew/cask-fonts
brew install font-hack-nerd-font

# Install exa
brew install exa &&

# Install vivid
brew install vivid &&

# Install powerlevel10k
cd ~/.zsh &&
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/.zsh/powerlevel10k &&

# Install fzf
git clone https://github.com/junegunn/fzf.git ~/.fzf &&
yes | ~/.fzf/install
