vim.cmd[[packadd packer.nvim]]

require'packer'.startup(function()
    -- General
    use'wbthomason/packer.nvim'

    -- Visual related
    ---- *nvim-web-devicions: show devicons
    use'kyazdani42/nvim-web-devicons'
    ---- *bufferline: show top bar
    use{'akinsho/bufferline.nvim', tag="*", requires='nvim-tree/nvim-web-devicons'}
    ---- lualine: show bottom bar
    use{'nvim-lualine/lualine.nvim', requires={'kyazdani42/nvim-web-devicons', opt=true}}
    ---- nvim-notify: better visualization of notifications
    -- use'rcarriga/nvim-notify'
    ---- indent-blankline: show indentation levels with lines
    -- use'lukas-reineke/indent-blankline.nvim'

    -- Editor
    ---- *vim-better-whitespace: show white space with red blocks
    use'ntpeters/vim-better-whitespace'
    ---- *vim-expand-region: visually select increasingly larger regions of text using the same key combination.
    use'terryma/vim-expand-region'
    ---- *vim-visual-multi: edit multiple positions simultaneously
    use'mg979/vim-visual-multi'
    ---- *nvim-cursorline: underlines the same words the cursor is on
    use'yamatsum/nvim-cursorline'
    ---- *hlargs: highlihgt argument definitions and usages
    use'm-demare/hlargs.nvim'

    -- Search
    ---- nvim-hlslens: show position of search
    -- use'kevinhwang91/nvim-hlslens'
    ---- searchbox: better visualizatin of searchbox
    -- use{'VonHeikemen/searchbox.nvim', requires={'MunifTanjim/nui.nvim'}}
    ---- pounce.nvim: incremental search
    use'rlane/pounce.nvim'

    -- Filer
    ---- *neo-tree: show file tree view
    use{'nvim-neo-tree/neo-tree.nvim',
        branch='v2.x',
        requires={'nvim-lua/plenary.nvim', 'kyazdani42/nvim-web-devicons', 'MunifTanjim/nui.nvim' }
    }
    ---- *telescope: fuzzy finders
    use{'nvim-telescope/telescope.nvim', requires='nvim-lua/plenary.nvim'}
    use'nvim-telescope/telescope-media-files.nvim'
    use'nvim-telescope/telescope-fzy-native.nvim'

    -- Movement
    ---- *vim-tmux-navigator: Seamless navigation between vim and tmux
    use'christoomey/vim-tmux-navigator'
    ---- marks.nvim: A better user experience for interacting with and manipulating Vim marks.
    -- use'chentoast/marks.nvim'

    -- Colorscheme
    ---- *nightfox: colorscheme
    use'EdenEast/nightfox.nvim'
    ---- tokyonight: colorscheme (TODO: toggle between tokyo-storm and tokyo-night)
    use'folke/tokyonight.nvim'
    ---- styler: Simple Neovim plugin to set a different colorscheme per filetype.
    -- use{'folke/styler.nvim',
    --     config = function()
    --         require('styler').setup()
    --     end
    -- }

    -- Visuals
    ---- nvim-scrollbar: simple visualization of scrollbar
    -- use'petertriho/nvim-scrollbar'
    ---- mode: change line color based on mode
    -- use{'mvllow/modes.nvim', tag='v0.2.0',
    --     config = function()
    --         require('modes').setup()
    --     end
    -- }

    -- Comments
    ---- Comment: toggle comment/uncomment state
    use'numToStr/Comment.nvim'
    ---- todo-comments: highlight certain keywords with different colors
    use{'folke/todo-comments.nvim', requires='nvim-lua/plenary.nvim', opts={}}

    -- Mason
    ---- *Mason: package manager for neovim (lsp servers, dap servers, linters, formatters
    use'williamboman/mason.nvim'
    ---- *Mason-lsp-config: manage link with lspconfig
    use'williamboman/mason-lspconfig.nvim'
    ---- *Mason-nvim-dap:  bridges mason.nvim with the nvim-dap plugin
    -- use'jay-babu/mason-nvim-dap.nvim'
    ---- *Mason-tool-installer:  Autoinstall mason packages
    use'WhoIsSethDaniel/mason-tool-installer.nvim'

    -- LSP
    ---- *nvim-cmp: A completion engine plugin for neovim
    use'hrsh7th/nvim-cmp'
    ---- *nvim-lspconfig: A suite of config files for LSP
    use'neovim/nvim-lspconfig'
    ---- *cmp-nvim-lsp: nvim-cmp source for neovim's built-in language server client
    use'hrsh7th/cmp-nvim-lsp'
    ---- cmp-nvim-lsp-signature-help: nvim-cmp source for displaying function signatures with the current parameter emphasized
    use'hrsh7th/cmp-nvim-lsp-signature-help'
    ---- cmp-cmdline: nvim-cmp source for vim's cmdline.
    use'hrsh7th/cmp-cmdline'
    ---- cmp-cmdline: nvim-cmp source for path
    use'hrsh7th/cmp-path'
    ---- cmp-treesitter: nvim-cmp source from treesitter
    use'ray-x/cmp-treesitter'
    ---- lspkind: show vscode-like pictograms in lsp
    -- use'onsails/lspkind.nvim'
    ---- nvim-cmp: source for nvim-dap REPL and nvim-dap-ui buffers
    -- use'rcarriga/cmp-dap'
    ---- cmp-tabnine: tabnine source for nvim-cmp
    -- use{'tzachar/cmp-tabnine', run='./install.sh', requires='hrsh7th/nvim-cmp'}
    ---- cmp_luasnip: luasnip completion source for nvim-cmp
    use'saadparwaiz1/cmp_luasnip'

    -- LSP plugins
    ---- *lspsaga: improved lsp UI
    -- https://zenn.dev/botamotch/articles/c02c51cff7d61d
    -- use{'nvimdev/lspsaga.nvim',
    --     config = function()
    --         require('lspsaga').setup({})
    --     end,
    -- }
    ---- *treesitter: linter
    use'nvim-treesitter/nvim-treesitter'
    ---- *lsp-colors: coloring diagnostics
    use'folke/lsp-colors.nvim'
    ---- *trouble: pretty list for showing diagnostics, references, telescope results
    -- use{'folke/trouble.nvim', requires='kyazdani42/nvim-web-devicons'}

    -- Git
    ---- *neogit: Git controll inside neovim
    use{'TimUntersberger/neogit', requires='nvim-lua/plenary.nvim'}
    ---- *diffview: Git diff/merge/history visualization
    use{'sindrets/diffview.nvim', requires='nvim-lua/plenary.nvim'}
    ---- *git-conflict: Highlight conflict areas
    use'akinsho/git-conflict.nvim'
    ---- *gitsigns: Show git decorations
    use'lewis6991/gitsigns.nvim'

    -- DAP (Debug Adapter Protocol)
    -- nvim-dap: DAP client for neovim
    -- use'mfussenegger/nvim-dap'
    -- nvim-dap-ui: DAP UI neovim
    -- use{'rcarriga/nvim-dap-ui', requires={'mfussenegger/nvim-dap', 'mfussenegger/nvim-dap-python', 'nvim-treesitter/nvim-treesitter'}}
    -- nvim-dap-virtual-text: Show virtual text during DAP
    -- use'theHamsta/nvim-dap-virtual-text'

    -- Snippets
    use{'L3MON4D3/LuaSnip', tag='v2.0.0', run='make install_jsregexp'}

    -- Terminal
    use{'akinsho/toggleterm.nvim', tag='*',
        config=function()
            require('toggleterm').setup()
        end
    }
end)
