#!/bin/bash

# Install dependencies
apt install -y automake bison build-essential pkg-config libevent-dev libncurses5-dev &&

# Clone and Install tmux
git clone https://github.com/tmux/tmux /usr/local/src/tmux &&
cd /usr/local/src/tmux &&
./autogen.sh &&
./configure --prefix=/usr/local &&
make &&
make install &&

# Clone tpm
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
