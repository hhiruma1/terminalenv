#!/bin/bash

# Download nvm
sudo -E wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | zsh &&

# Load nvm
export NVM_DIR="$HOME/.config/nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion


# Add nvm settings to .profile
echo 'export NVM_DIR="$HOME/.config/nvm"' >> ~/.profile &&
echo '[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"' >> ~/.profile &&
echo '[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"' >> ~/.profile &&

export NVM_DIR="$HOME/.config/nvm" &&
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" &&
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" &&

# Install node
nvm install 18.20.4 &&
# copy new node related lines on .profile to .zshrc
npm install --global neovim &&
ln -s ~/.config/nvm/versions/node/v18.20.4 ~/.config/nvm/versions/node/latest
