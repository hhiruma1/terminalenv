-----------------------
-- Key Mapping
-----------------------
local keymap = vim.api.nvim_set_keymap

--Leader
vim.g.mapleader = ' '

--Split view
keymap('n', '<Leader>s', '<cmd>split<CR>', {noremap=true, desc='Split window horizontally'})
keymap('n', '<Leader>v', '<cmd>vsplit<CR>', {noremap=true, desc='Split window vertically'})

--Toggle NERDTree
keymap('n', '<Leader>n', '<cmd>Neotree toggle<CR>', {noremap=true, silent=true, desc='Toggle Neotree'})

--Move between buffers
keymap('n', '<Leader>[', '<cmd>bprev<CR>', {noremap=true, silent=true, desc='To previous buffer'})
keymap('n', '<Leader>]', '<cmd>bnext<CR>', {noremap=true, silent=true, desc='To next buffer'})

--Move between tabs
keymap('n', '<Leader>;', '<cmd>tabprev<CR>', {noremap=true, silent=true, desc='To previous tab'})
keymap('n', '<Leader>\'', '<cmd>tabnext<CR>', {noremap=true, silent=true, desc='To next tab'})
keymap('n', '<Leader>tn', '<cmd>tabnew<CR>', {noremap=true, silent=true, desc='Create new tab'})
keymap('n', '<Leader>tc', '<cmd>tabclose<CR>', {noremap=true, silent=true, desc='Close current tab'})

--Delete buffer
keymap('n', '<Leader>dd', '<cmd>bd<CR>', {noremap=true, silent=true, desc='Delete buffer'})
keymap('n', '<Leader>DD', '<cmd>BufferLineCloseOthers<CR>', {noremap=true, silent=true, desc='Delete all other buffers'})

--Move visual lines
keymap('n', 'j', 'gj', {noremap=true})
keymap('n', 'k', 'gk', {noremap=true})

--Move back to previous edit
keymap('n', '<Leader>z', '<cmd>lua require("before").jump_to_last_edit()<cr>', {noremap=true})
keymap('n', '<Leader>q', '<cmd>lua require("before").jump_to_next_edit()<cr>', {noremap=true})
keymap('n', '<Leader>fe', '<cmd>lua require("before").show_edits_in_telescope()<cr>', {noremap=true})

--Expand visual region
keymap('v', 'v', '<Plug>(expand_region_expand)', {noremap=true})
keymap('v', 'V', '<Plug>(expand_region_shrink)', {noremap=true})

--Resize windows
keymap('n', '<Leader>w', '<C-w>=', {noremap=true, desc='Resize all windows'})

--Telescope
keymap('n', '<Leader>ff', '<cmd>Telescope find_files prompt_prefix=🔍 <cr>', {noremap=true, desc='Telescope: file'})
keymap('n', '<Leader>fg', '<cmd>Telescope live_grep prompt_prefix=🔍 <cr>', {noremap=true, desc='Telescope: word'})
keymap('n', '<Leader>fb', '<cmd>Telescope buffers prompt_prefix=🔍 <cr>', {noremap=true, desc='Telescope: buffer'})
keymap('n', '<Leader>fh', '<cmd>Telescope diagnostics prompt_prefix=🔍 <cr>', {noremap=true, desc='Telescope: diagnostics'})
keymap('n', '<Leader>fr', '<cmd>Telescope lsp_references prompt_prefix=🔍 <cr>', {noremap=true, desc='Telescope: lsp'})
keymap('n', '<Leader>f/', '<cmd>Telescope current_buffer_fuzzy_find prompt_prefix=🔍 <cr>', {noremap=true, desc='Telescope: word (in buffer)'})
keymap('n', '<Leader>fk', '<cmd>Telescope keymaps prompt_prefix=🔍 <cr>', {noremap=true, desc='Telescope: keymaps'})
keymap('n', '<Leader>fd', '<cmd>Telescope find_files cwd=~/terminalenv/dotfiles hidden=true <cr>', {noremap=true, desc='Telescope: dotfiles'})
keymap('n', '<Leader>fv', '<cmd>Telescope find_files cwd=~/envtips <cr>', {noremap=true, desc='Telescope: tips'})
keymap('n', '<Leader>fT', '<cmd>TodoTelescope <cr>', {noremap=true, desc='Telescope: TODOs'})
keymap('n', '<Leader>ft', '<cmd>lua require("telescope-toggleterm").open()<cr>', {noremap=true, desc='Toggle terminal'})
keymap('n', '<Leader>fn', '<cmd>lua require("telescope").extensions.notify.notify()<cr>', {noremap=true, desc='Notify'})

--LSP
keymap('n', 'K', '<cmd>Lspsaga hover_doc<CR>', {noremap=true})
keymap('n', 'e]', '<cmd>Lspsaga diagnostic_jump_next<CR>', {noremap=true})
keymap('n', 'e[', '<cmd>Lspsaga diagnostic_jump_prev<CR>', {noremap=true})
keymap("n", "<LEADER>jd", "<cmd>Lspsaga goto_definition<CR>", {noremap=true, desc='Saga: goto def'})
keymap("n", "<LEADER>jp", "<cmd>Lspsaga peek_type_definition<CR>", {noremap=true, desc='Saga: peek type'})
keymap("n", "<LEADER>jD", "<cmd>Lspsaga peek_definition<CR>", {noremap=true, desc='Saga: peek def'})
keymap("n", "<LEADER>jo", "<cmd>Lspsaga outline<CR>", {noremap=true, desc='Saga: outline'})
keymap("n", "<LEADER>l", "<cmd>lua require('lsp_lines').toggle() <CR>", {noremap=true, desc='Toggle lsp line'})

--Git
keymap('n', '<leader>gg', '<cmd>Neogit<cr>', {noremap=true, desc='Git: open'})
keymap('n', '<leader>gdd', '<cmd>diffviewopen<cr>', {noremap=true, desc='Git: diffview'})
keymap('n', '<leader>gdh', '<cmd>DiffviewFileHistory<cr>', {noremap=true, desc='Git: file history'})
keymap('n', '<leader>gsl', '<cmd>gitsigns.toggle_linehl<cr>', {noremap=true, desc='Git: line highlight'})
keymap('n', '<leader>gsw', '<cmd>gitsigns.toggle_word_diff<cr>', {noremap=true, desc='Git: word diff'})
keymap('n', '<leader>gsd', '<cmd>gitsigns.toggle_deleted<cr>', {noremap=true, desc='Git: deleted'})

--DAP
keymap('n', '<leader>du', '<cmd>lua require"dapui".toggle()<cr>', {noremap=true, desc='Dap: open'})
keymap('n', '<leader>c', '<cmd>lua require"dap".continue()<cr>', {noremap=true, desc='Dap: continue'})
keymap('n', '<leader>dl', '<cmd>lua require"dap.ext.vscode".load_launchjs("dap_launch.json")<cr>', {noremap=true, desc='Dap: launch'})
keymap('n', '<leader>db', '<cmd>lua require"dap".toggle_breakpoint()<cr>', {noremap=true, desc='Dap: Breakpoint'})
keymap('n', '<leader>do', '<cmd>lua require"dap".step_over()<cr>', {noremap=true, desc='Dap: Step over'})
keymap('n', '<leader>dr', '<cmd>lua require"dap".step_back()<cr>', {noremap=true, desc='Dap: Step back'})
keymap('n', '<leader>dt', '<cmd>lua require"dap".terminate()<cr>', {noremap=true, desc='Dap: Terminate'})

--Terminal
keymap('n', '<leader>tt', '<cmd>ToggleTerm direction=vertical<cr>', {noremap=true, desc='Term: vsplit'})
keymap('n', '<leader>th', '<cmd>ToggleTerm direction=horizontal<cr>', {noremap=true, desc='Term: hsplit'})
keymap('n', '<leader>tf', '<cmd>ToggleTerm direction=float<cr>', {noremap=true, desc='Term: float'})
keymap('n', '<leader>tr', '<cmd>ToggleTermSetName<cr>', {noremap=true, desc='Term: set name'})
keymap('n', '<leader>ts', '<cmd>TermSelect<cr>', {noremap=true, desc='Term: select'})
keymap('n', '<leader>1', '<cmd>1ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('n', '<leader>2', '<cmd>2ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('n', '<leader>3', '<cmd>3ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('n', '<leader>4', '<cmd>4ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('n', '<leader>5', '<cmd>5ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('n', '<leader>6', '<cmd>6ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('n', '<leader>7', '<cmd>7ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('n', '<leader>8', '<cmd>8ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('n', '<leader>9', '<cmd>9ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('n', '<leader>0', '<cmd>0ToggleTerm direction=vertical<cr>', {noremap=true})
keymap('t', '<C-h>', '<C-\\><C-n><C-w><C-h>', {noremap=true})
keymap('t', '<C-j>', '<C-\\><C-n><C-w><C-j>', {noremap=true})
keymap('t', '<C-k>', '<C-\\><C-n><C-w><C-k>', {noremap=true})
keymap('t', '<C-l>', '<C-\\><C-n><C-w><C-l>', {noremap=true})
keymap('t', '<A-[>', '<C-\\><C-n>', {noremap=true})
