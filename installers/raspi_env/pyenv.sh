#!/bin/bash

# Install dependencies
sudo -E apt install -y python3-tk zlib1g-dev libssl-dev &&
sudo -E apt install -y liblzma-dev libsqlite3-dev libffi-dev libreadline-dev libbz2-dev &&

# Clone pyenv
git clone https://github.com/pyenv/pyenv.git ~/.pyenv &&

# Install python
export PYENV_ROOT="$HOME/.pyenv" &&
export PATH="$PYENV_ROOT/bin:$PATH" &&
export PATH="$PYENV_ROOT/shims:$PATH" &&

echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.profile &&
echo 'export PATH="$PYENV_ROOT/bin:$PATH" &&' >> ~/.profile &&
echo 'export PATH="$PYENV_ROOT/shims:$PATH" &&' >> ~/.profile &&
pyenv install 3.10.10 &&
pyenv global 3.10.10 &&

# Install pyenv-virtualenv
git clone https://github.com/yyuu/pyenv-virtualenv.git ~/.pyenv/plugins/pyenv-virtualenv
