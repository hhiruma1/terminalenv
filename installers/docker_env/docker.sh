#!/bin/bash

apt update &&
apt install -y ca-certificates curl gnupg lsb-release &&

# Install docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg &&

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null &&

apt update &&
apt install -y docker-ce docker-ce-cli containerd.io &&
usermod -aG docker $USER &&

cat <<EOF >> ~/.profile
# For Docker service start
/etc/init.d/docker start
EOF &&

# Install docker compose
curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose &&
chmod +x /usr/local/bin/docker-compose &&
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
