local wezterm = require 'wezterm'

return {
  default_prog = {"C:\\Windows\\System32\\wsl.exe", "--distribution", "Ubuntu-18.04", "--exec", "/bin/zsh", "-l"},
  color_scheme = "iceberg-dark",
  font_size = 10.0,
  leader = { key="Space", mods="CTRL", timeout_milliseconds=1000 },
  keys = {
      -- Split panes
      { key="v", mods="LEADER", action=wezterm.action{ SplitHorizontal={ domain="CurrentPaneDomain"} } },
      { key="s", mods="LEADER", action=wezterm.action{ SplitVertical={ domain="CurrentPaneDomain"} } },

      -- Move between panes
      { key="h", mods="CTRL", action=wezterm.action{ ActivatePaneDirection="Left" }},
      { key="l", mods="CTRL", action=wezterm.action{ ActivatePaneDirection="Right" }},
      { key="j", mods="CTRL", action=wezterm.action{ ActivatePaneDirection="Down" }},
      { key="k", mods="CTRL", action=wezterm.action{ ActivatePaneDirection="Up" }},

      -- Resize panes
      { key="y", mods="CTRL", action=wezterm.action{ AdjustPaneSize={"Left", 3}}},
      { key="o", mods="CTRL", action=wezterm.action{ AdjustPaneSize={"Right", 3}}},
      { key="u", mods="CTRL", action=wezterm.action{ AdjustPaneSize={"Down", 3}}},
      { key="i", mods="CTRL", action=wezterm.action{ AdjustPaneSize={"Up", 3}}},

      -- Create/Delete/Move tab
      { key="c", mods="LEADER", action=wezterm.action{ SpawnTab="CurrentPaneDomain" }},
      { key="n", mods="LEADER", action=wezterm.action{ActivateTabRelative=1}},
      { key="p", mods="LEADER", action=wezterm.action{ActivateTabRelative=-1}},

      -- Reload configuration
      { key="r", mods="LEADER", action="ReloadConfiguration"},

  },
  scrollback_lines = 3500,
}
