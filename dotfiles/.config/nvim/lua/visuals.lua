-----------------------
-- Basic visual configurations
-----------------------

--Line numbers
vim.opt.number = true
vim.wo.signcolumn = "yes"

--Enable cursor to move one char ahead
vim.opt.virtualedit = 'onemore'

--Smartindent
vim.opt.smartindent = true

--Matching brackets
vim.opt.showmatch = true

--show folder icons
vim.g.WebDevIconsUnicodeDecorateFolderNodes = 1

--Use opened buffer instead of recreating
vim.opt.switchbuf = 'useopen'

--Show currently inputting commands
vim.opt.showcmd = true

--Show current input mode
vim.opt.showmode = true

--Colorscheme
-- vim.api.nvim_command [[colorscheme nightfox]]

--Darkmode
vim.opt.background = 'dark'

--Enable syntax highlight
vim.api.nvim_command [[syntax on]]

--Disable wrapping
vim.opt.wrap = false

--Termguicolors
vim.opt.termguicolors = true

-----------------------
-- Plugin visual configurations
-----------------------

require('lualine').setup{
    options={
        component_separators={ left = '', right = '' },
        section_separators={ left = '', right = '' },
    },
    sections = {
        lualine_a = {'mode'},
        lualine_b = {'branch', 'diff', 'diagnostics'},

        lualine_c = {
            {
                'filename',
                file_status = true,      -- Displays file status (readonly status, modified status)
                newfile_status = false,  -- Display new file status (new file means no write after created)
                path = 2,                -- 0: Just the filename
                                         -- 1: Relative path
                                         -- 2: Absolute path
                                         -- 3: Absolute path, with tilde as the home directory

                shorting_target = 40,    -- Shortens path to leave 40 spaces in the window
                                         -- for other components. (terrible name, any suggestions?)
                symbols = {
                    modified = '[+]',      -- Text to show when the file is modified.
                    readonly = '[-]',      -- Text to show when the file is non-modifiable or readonly.
                    unnamed = '[No Name]', -- Text to show for unnamed buffers.
                    newfile = '[New]',     -- Text to show for new created file before first writting
                }
            }
        },


        lualine_x = {'encoding', 'fileformat', 'filetype'},
        lualine_y = {'progress'},
        lualine_z = {'location'}
    },
    inactive_sections = {
        lualine_a = {},
        lualine_b = {},
        lualine_c = {'filename'},
        lualine_x = {'location'},
        lualine_y = {},
        lualine_z = {}
    },
    tabline = {},
    extensions = {}
}

require("scope").setup({
    hooks = {
    },
})
require("telescope").load_extension("scope")

require("bufferline").setup{
    options={
        diagnostics='nvim_lsp',
        update_in_insert=true,
        diagnostics_indicator = function(count, level, diagnostics_dict, context)
            local icon = level:match("error") and " " or " "
            return " " .. icon .. count
        end,
        offsets = {{filetype="neo-tree", text="file explorer", text_align="left"}},
    }
}

require("ibl").setup {
}

require("noice").setup({
  lsp = {
    -- override markdown rendering so that **cmp** and other plugins use **Treesitter**
    override = {
      ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
      ["vim.lsp.util.stylize_markdown"] = true,
      ["cmp.entry.get_documentation"] = true,
    },
  },
  -- you can enable a preset for easier configuration
  presets = {
    bottom_search = false, -- use a classic bottom cmdline for search
    command_palette = true, -- position the cmdline and popupmenu together
    long_message_to_split = true, -- long messages will be sent to a split
    inc_rename = false, -- enables an input dialog for inc-rename.nvim
    lsp_doc_border = false, -- add a border to hover docs and signature help
  },
})
