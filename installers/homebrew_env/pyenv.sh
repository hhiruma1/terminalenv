#!/bin/bash

# Install dependencies
brew install xz &&

# Clone pyenv
git clone https://github.com/pyenv/pyenv.git ~/.pyenv &&

# Install python
export PYENV_ROOT="$HOME/.pyenv" &&
export PATH="$PYENV_ROOT/bin:$PATH" &&
export PATH="$PYENV_ROOT/shims:$PATH" &&
pyenv install 3.10.10 &&
pyenv global 3.10.10 &&

# Install pyenv-virtualenv
git clone https://github.com/yyuu/pyenv-virtualenv.git ~/.pyenv/plugins/pyenv-virtualenv
