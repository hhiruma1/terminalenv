-----------------------
-- Editing related
-----------------------
--Visualize unseen characters
vim.opt.listchars = {tab='▸-'}

--Convert tab as multiple spaces
vim.opt.expandtab = true

--Other configs
vim.opt.tabstop = 2
vim.opt.shiftwidth = 4
vim.opt.autoindent = true
vim.opt.smartindent = true

--Enable backspace
vim.opt.backspace = {'indent', 'eol', 'start'}

--Set clipboard
vim.opt.clipboard:append{'unnamedplus'}

-----------------------
-- Search related
-----------------------
--Ignore letter cases when searched with all lower cases
vim.opt.ignorecase = true

--Differentiate case when searched including upper cases
vim.opt.smartcase = true

--Highlight during input
vim.opt.incsearch = true

--Return when reached the EOF
vim.opt.wrapscan = true

--Highlight targets
vim.opt.hlsearch = true

--Escape to end highlight
vim.api.nvim_set_keymap('n', '<Esc><Esc>', ':nohlsearch<CR>', {})


-----------------------
-- Other settings
-----------------------
--mouse control
vim.opt.mouse = 'a'

--Round shifting using < and > to shiftwidth
vim.opt.shiftround = true

--Ignore case on completion
vim.opt.infercase = true

--set file encodings
vim.opt.fileencodings = 'utf-8', 'cp932', 'euc-jp', 'sjis'

--Do not create backups or swapfiles
vim.opt.writebackup = false
vim.opt.backup = false
vim.opt.swapfile = false

--Auto reload file if edited
vim.opt.autoread = true

--Disable beep sound
vim.opt.belloff = 'all'
