vim.cmd[[packadd packer.nvim]]

require'packer'.startup(function()
    -- General
    use'wbthomason/packer.nvim'

    -- Movement realted
    use'petertriho/nvim-scrollbar'
    use'kevinhwang91/nvim-hlslens'
    -- use'unblevable/quick-scope'

    -- Edit related
    use'ntpeters/vim-better-whitespace'
    use'christoomey/vim-tmux-navigator'
    use{'folke/todo-comments.nvim', requires='nvim-lua/plenary.nvim'}
    use{'mvllow/modes.nvim', tag='v0.2.0',
        config = function()
            require('modes').setup()
        end
    }
    use'kana/vim-niceblock'
    use'numToStr/Comment.nvim'
    use'rmagatti/goto-preview'
    use{'VonHeikemen/searchbox.nvim', requires={'MunifTanjim/nui.nvim'}}

    -- use'ervandew/supertab'
    use'terryma/vim-expand-region'
    use'mg979/vim-visual-multi'
    use'yamatsum/nvim-cursorline'

    -- Visual related
    use'EdenEast/nightfox.nvim'
    use'kyazdani42/nvim-web-devicons'
    use'ryanoasis/vim-devicons'
    -- use'vim-airline/vim-airline-themes'
    use{'nvim-lualine/lualine.nvim', requires={'kyazdani42/nvim-web-devicons', opt=true}}
    use'folke/tokyonight.nvim'
    use {'romgrk/barbar.nvim', requires = 'nvim-web-devicons'}
    use'rcarriga/nvim-notify'
    use'lukas-reineke/indent-blankline.nvim'

    -- Git
    use'tpope/vim-fugitive'
    use{'TimUntersberger/neogit', requires='nvim-lua/plenary.nvim'}
    use{'sindrets/diffview.nvim', requires='nvim-lua/plenary.nvim'}
    use'akinsho/git-conflict.nvim'
    -- use'rhysd/committia.vim'
    use'lewis6991/gitsigns.nvim'

    -- filer
    use{'nvim-neo-tree/neo-tree.nvim', branch='v2.x', requires={'nvim-lua/plenary.nvim', 'kyazdani42/nvim-web-devicons', 'MunifTanjim/nui.nvim' }}

    -- fuzzy finders
    use{'nvim-telescope/telescope.nvim', requires='nvim-lua/plenary.nvim'}

    -- treesitter
    use'nvim-treesitter/nvim-treesitter'

    -- sidebar
    use'sidebar-nvim/sidebar.nvim'
    use'sidebar-nvim/sections-dap'

    -- Outline
    use'stevearc/aerial.nvim'

    -- LSP and autofill
    use'hrsh7th/nvim-cmp'
    use'hrsh7th/cmp-buffer'
    use'hrsh7th/cmp-calc'
    use'hrsh7th/cmp-cmdline'
    -- use'hrsh7th/cmp-emoji'
    use'hrsh7th/cmp-nvim-lsp'
    use'hrsh7th/cmp-nvim-lsp-signature-help'
    use'hrsh7th/cmp-path'
    use'hrsh7th/cmp-nvim-lua'
    use'neovim/nvim-lspconfig'
    use'onsails/lspkind.nvim'
    use'ray-x/cmp-treesitter'
    use'rcarriga/cmp-dap'
    use'williamboman/mason.nvim'
    use'williamboman/mason-lspconfig.nvim'
    use'jay-babu/mason-nvim-dap.nvim'
    use {'tzachar/cmp-tabnine', run='./install.sh', requires = 'hrsh7th/nvim-cmp'}

    -- LSP related UI
    use'tami5/lspsaga.nvim'
    use'folke/lsp-colors.nvim'
    use{'j-hui/fidget.nvim',
        config=function()
            require('fidget').setup{}
        end
    }
    use{'folke/trouble.nvim', requires='kyazdani42/nvim-web-devicons'}

    -- debug
    use'mfussenegger/nvim-dap'
    use{'rcarriga/nvim-dap-ui', requires={'mfussenegger/nvim-dap', 'mfussenegger/nvim-dap-python', 'nvim-treesitter/nvim-treesitter'}}
    use'theHamsta/nvim-dap-virtual-text'

    -- terminal
    use{'akinsho/toggleterm.nvim', tag = '*'}

    -- Runner
    use{'michaelb/sniprun', run='bash ./install.sh'}

    -- Others
    use'tyru/open-browser.vim'
    use'tyru/open-browser-github.vim'

end)
