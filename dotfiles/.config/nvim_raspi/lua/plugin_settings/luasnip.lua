local ls = require('luasnip')
local snip = ls.snippet
local text = ls.text_node
local insert = ls.insert_node

ls.add_snippets(nil, {
    python = {
        snip({
            trig = 'np',
        }, {
            text({'import numpy as np', ''}),
            insert(0),
        }),
        snip({
            trig = 'plt',
        }, {
            text({'import matplotlib.pyplot as plt', 'import matplotlib', ''}),
            insert(0),
        }),
        snip({
            trig = 'torch',
        }, {
            text({'import torch', 'import torch.nn as nn', 'import torch.nn.functional as F', ''}),
            insert(0),
        }),
        snip({
            trig = 'anim',
        }, {
            text({
                'import matplotlib.animation as anim',
                'fig, ax = plt.subplot(1, 1)',
                'def anim_update(i):',
                '    ...',
                'ani = anim.FuncAnimation(fig, anim_update, interval=100, frames=imgs.shape[0])',
                'ani.save("")',
                ''
            }),
            insert(0),
        }),
    },
})
